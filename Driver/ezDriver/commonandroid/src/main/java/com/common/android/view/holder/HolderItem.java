package com.common.android.view.holder;

import android.view.View;

public abstract class HolderItem<T> extends HolderFrag {

    public HolderItem(final View view) {
        super(view);
    }

    /*
     * Puts values on components.
     *
     * @param T A class like a pojo to set the values.
     */
    public abstract void putValues(final T value);

}
