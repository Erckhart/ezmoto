package com.common.android.app;

import android.app.Application;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

public class CommonApplication extends Application {

    private List<AsyncTask<?,?,?>> mAppTask;

    @Override
    public void onCreate() {
        super.onCreate();
        mAppTask = new ArrayList<AsyncTask<?, ?, ?>>();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        for (AsyncTask<?,?,?> task : mAppTask) {
            task.cancel(true);
        }
    }

    public void addTask(final AsyncTask<?,?,?> task) {
        if(task != null) {
            try {
                mAppTask.add(task);
            }catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        }
    }

    public void removeTask(final AsyncTask<?,?,?> task) {
        if(task != null) {
            try {
                mAppTask.remove(task);
            }catch (UnsupportedOperationException exc) {
                exc.printStackTrace();
            }
        }
    }

}
