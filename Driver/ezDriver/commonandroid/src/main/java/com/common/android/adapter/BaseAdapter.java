package com.common.android.adapter;

import java.util.List;

import android.content.Context;

public abstract class BaseAdapter<T> extends android.widget.BaseAdapter {

    private List<T> items;
    private final Context context;

    public BaseAdapter(final Context context) {
        this.context = context;
    }

    public BaseAdapter(final List<T> source, final Context context) {
        this(context);
        this.items = source;
    }

    @Override
    public int getCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    @Override
    public T getItem(final int position) {
        if (!hasItems() || position >= items.size() || position < 0) {
            return null;
        }
        return items.get(position);
    }

    @Override
    public long getItemId(final int position) {
        return position;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(final List<T> items) {
        this.items = items;
    }

    protected Context getContext() {
        return context;
    }

    public boolean hasItems() {
        return items != null && !items.isEmpty();
    }

}
