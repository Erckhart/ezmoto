package com.common.android.view.holder;

import android.content.Context;
import android.view.View;

public abstract class HolderFrag {

    protected final View mView;

    public HolderFrag(final View view) {
        mView = view;
        initializeFields();
    }

    /**
     * initialize fields and finds
     */
    protected abstract void initializeFields();

    /**
     * performing the search components from the interface implemented by any
     * activity
     *
     * @param resId
     * @return {@link View}
     */
    protected View getField(final int resId) {
        return mView.findViewById(resId);
    }

    protected Context getContext() {
        return mView.getContext();
    }

}
