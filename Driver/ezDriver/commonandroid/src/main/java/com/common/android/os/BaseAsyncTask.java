package com.common.android.os;

import android.content.Context;
import android.os.AsyncTask;

import com.common.android.app.CommonApplication;

public abstract class BaseAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private final Context mContext;

    public BaseAsyncTask(final Context context) {
        mContext = context;
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(isCommonApp()) {
            ((CommonApplication) mContext.getApplicationContext()).addTask(this);
        }
    }

    @Override
    protected void onPostExecute(final Result result) {
        super.onPostExecute(result);
        if(isCommonApp()) {
            ((CommonApplication) mContext.getApplicationContext()).addTask(this);
        }
    }

    private boolean isCommonApp() {
        return mContext.getApplicationContext() instanceof CommonApplication;
    }

}
