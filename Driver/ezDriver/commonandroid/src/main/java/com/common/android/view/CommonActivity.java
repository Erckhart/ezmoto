package com.common.android.view;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public abstract class CommonActivity extends AppCompatActivity implements ISupportView {

    @Override
    public void onBackPressed() {
        final CommonFragment fragment = getActiveFragment();
        if(fragment == null || !fragment.onBackPressed()) {
            super.onBackPressed();
        }
    }

    public CommonFragment getActiveFragment() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            return null;
        }
        final String tag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        return (CommonFragment) getSupportFragmentManager().findFragmentByTag(tag);
    }

    @Override
    public void navigateTo(final Class<?> to) {
        if (to != null) {
            startActivity(new Intent(this, to));
        }
    }

    @Override
    public void navigateTo(final int containerID, final Fragment fragment, final boolean hasToStack) {
        if (fragment != null) {
            final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            transaction.replace(containerID, fragment);
            if (hasToStack) {
                transaction.addToBackStack(fragment.getClass().getName());
            }
            transaction.commitAllowingStateLoss();
        }
    }

    @Override
    public void clearNavigation() {
        final FragmentManager manager = getSupportFragmentManager();
        for (int i = 0; i < manager.getBackStackEntryCount(); i++) {
            final FragmentManager.BackStackEntry backStackEntry = manager.getBackStackEntryAt(i);
            manager.popBackStack(backStackEntry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}
