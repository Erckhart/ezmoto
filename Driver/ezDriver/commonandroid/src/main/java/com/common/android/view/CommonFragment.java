package com.common.android.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;

public abstract class CommonFragment extends Fragment implements ISupportFragView {

    private ISupportView mSupportView;

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        if(context instanceof Activity && context instanceof ISupportView) {
            mSupportView = (ISupportView) context;
        }
    }

    @Override
    public ISupportView getSupportView() {
        return mSupportView;
    }

    /**
     * An alternative to Activity#onBackPressed() which brings to fragment instance. It should return false if there is no handle.
     * @return True if the back press was handled otherwise false to call the Activity#onBackPressed().
     */
    @Override
    public boolean onBackPressed() {
        return false;
    }

}
