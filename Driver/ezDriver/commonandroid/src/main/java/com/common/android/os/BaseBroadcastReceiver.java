package com.common.android.os;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;

public abstract class BaseBroadcastReceiver extends BroadcastReceiver {

    protected static final String TAG = BaseBroadcastReceiver.class.getSimpleName();

    private boolean isEnabled;

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(final Context context, final boolean enable) {
        try {
            if (context == null) {
                return;
            } else if (enable) {
                context.registerReceiver(this, getFilter());
            } else if(isEnabled) {
                context.unregisterReceiver(this);
            }
            isEnabled = enable;
        } catch (IllegalArgumentException e) {
            Log.e(TAG, e.getMessage(), e);
        }
    }

    protected abstract IntentFilter getFilter();

}
