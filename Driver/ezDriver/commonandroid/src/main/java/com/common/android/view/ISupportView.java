package com.common.android.view;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

/*
 * An interface which possesses some methods from an {@link AppCompatActivity} to be used anywhere without need the Activity itself.
 */
public interface ISupportView {

    /**
     * @return {@link Context}
     * @see AppCompatActivity#getBaseContext()
     */
    Context getBaseContext();

    /**
     * @return {@link Context}
     * @see AppCompatActivity#getApplicationContext()
     */
    Context getApplicationContext();

    /**
     * @return {@link AssetManager}
     * @see AppCompatActivity#getAssets()
     */
    AssetManager getAssets();

    /**
     * @param resId
     * @return {@link String}
     * @see AppCompatActivity#getString(int)
     */
    String getString(final int resId);

    /**
     * @return {@link Resources}
     * @see AppCompatActivity#getResources()
     */
    Resources getResources();

    /**
     * @return {@link View}
     * @see AppCompatActivity#findViewById(int)
     */
    View findViewById(final int mId);

    /**
     * @see AppCompatActivity#getSupportFragmentManager()
     */
    FragmentManager getSupportFragmentManager();

    /**
     * @return {@link SharedPreferences}
     * @see AppCompatActivity#getSharedPreferences(String, int)
     */
    SharedPreferences getSharedPreferences(final String name, final int mode);

    /**
     * @see AppCompatActivity#startActivity(Intent)
     */
    void startActivity(final Intent intent);

    /**
     * @see AppCompatActivity#finish()
     */
    void finish();

    /**
     * @see AppCompatActivity#onBackPressed()
     */
    void onBackPressed();

    /**
     * @see AppCompatActivity#setResult(int, Intent)
     */
    void setResult(int resultCode, Intent data);

    /**
     * @see AppCompatActivity#getIntent()
     */
    Intent getIntent();

    /**
     * @see AppCompatActivity#setSupportActionBar(Toolbar)
     */
    void setSupportActionBar(final Toolbar toolbar);

    /**
     * @see AppCompatActivity#getSupportActionBar()
     */
    ActionBar getSupportActionBar();

    /**
     * @see AppCompatActivity#getContentResolver()
     */
    ContentResolver getContentResolver();

    /**
     * Initializes all actions for this view.
     */
    void initializeActions();

    /*
     * Navigates to another fragment.
     */
    void navigateTo(final int containerID, final Fragment fragment, final boolean hasToStack);

    /*
     * Navigates to another activity.
     */
    void navigateTo(final Class<?> to);

    /*
     * Clear all fragments on back stack.
     */
    void clearNavigation();

}