package com.common.android.view.holder;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.common.android.view.ISupportView;

public abstract class HolderAct {

    protected final ISupportView mView;

    public HolderAct(final ISupportView view) {
        mView = view;
        initializeFields();
    }

    /**
     * initialize fields and finds
     */
    protected abstract void initializeFields();

    /**
     * performing the search components from the interface implemented by any
     * activity
     *
     * @param resId
     * @return {@link View}
     */
    protected View getField(final int resId) {
        return mView.findViewById(resId);
    }

    /**
     * @param resId
     * @return {@link String}
     * @see Activity#getString(int)
     */
    public String getString(final int resId) {
        return mView.getString(resId);
    }

    /**
     * @return {@link Context}
     * @see Activity#getApplicationContext()
     */
    public Context getApplicationContext() {
        return mView.getApplicationContext();
    }

    public Context getBaseContext() {
        return mView.getBaseContext();
    }

}
