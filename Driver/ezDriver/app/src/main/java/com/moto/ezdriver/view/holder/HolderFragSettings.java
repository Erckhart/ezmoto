package com.moto.ezdriver.view.holder;

import android.support.v7.preference.Preference;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.common.android.view.holder.HolderFrag;
import com.moto.ezdriver.R;

public class HolderFragSettings extends HolderFrag {

    private Switch mAvailableTrip;
    private TextView mAvailableStatus;

    private Button mLogoutButton;

    private ImageView mProfilePhoto;

    public HolderFragSettings(final View view) {
        super(view);
    }

    @Override
    protected void initializeFields() {
        mAvailableTrip = (Switch) getField(R.id.frag_settings_sw_available_for_trip);
        mAvailableStatus = (TextView) getField(R.id.frag_settings_tv_available_status);
        mLogoutButton = (Button) getField(R.id.frag_settings_bt_logout);
        mProfilePhoto = (ImageView) getField(R.id.frag_settings_iv_profile_photo);
    }

    public Switch getAvailableTrip() {
        return mAvailableTrip;
    }

    public TextView getAvailableStatus() {
        return mAvailableStatus;
    }

    public Button getLogoutButton() {
        return mLogoutButton;
    }

    public ImageView getProfilePhoto() {
        return mProfilePhoto;
    }
}
