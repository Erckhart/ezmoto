package com.moto.ezdriver.view.holder;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.common.android.view.holder.HolderFrag;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.net.conn.param.SignUpParam;
import com.moto.ezdriver.controller.net.conn.param.UserParam;

import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class HolderFragSignUp extends HolderFrag {

    private EditText mName;
    private EditText mEmail;
    private EditText mPassword;

    private Button mCancel;
    private Button mNext;

    private MaterialProgressBar mLoading;

    public HolderFragSignUp(final View view) {
        super(view);
    }

    @Override
    protected void initializeFields() {
        mName = (EditText) getField(R.id.frag_sign_up_et_name);
        mEmail = (EditText) getField(R.id.frag_sign_up_et_email);
        mPassword = (EditText) getField(R.id.frag_sign_up_et_pass);
        mCancel = (Button) getField(R.id.frag_sign_up_bt_cancel);
        mNext = (Button) getField(R.id.frag_sign_up_bt_next);
        mLoading = (MaterialProgressBar) getField(R.id.frag_sign_up_loading);
    }

    public void setLoadingEnabled(final boolean enabled) {
        mLoading.setVisibility(enabled ? View.VISIBLE : View.GONE);
    }

    public Button getButtonCancel() {
        return mCancel;
    }

    public Button getButtonNext() {
        return mNext;
    }

    public EditText getPassword() {
        return mPassword;
    }

    public SignUpParam getSignUpInfo() {
        final String email = mEmail.getText().toString();
        final String name = mName.getText().toString();
        final String pass = mPassword.getText().toString();
        return new SignUpParam(new UserParam(name, pass, email));
    }

}
