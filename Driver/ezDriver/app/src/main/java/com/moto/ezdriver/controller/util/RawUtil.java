package com.moto.ezdriver.controller.util;

import android.content.Context;
import android.text.TextUtils;

import com.moto.ezdriver.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class RawUtil {

    public static final String toMD5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String encode(final String text) {
        return text.replaceAll("[AEIOUaeiou]","");
    }

    public static int getColor(final Context context, final int colorRes) {
        if(android.os.Build.VERSION.SDK_INT >= 23) {
            return context.getColor(colorRes);
        } else {
            return context.getResources().getColor(colorRes);
        }
    }

    public static boolean isEmptyNullOrWhitespace(final String text) {
        return TextUtils.isEmpty(text) || text.trim().length() == 0;
    }

}
