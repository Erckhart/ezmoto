package com.moto.ezdriver.controller;

import android.view.View;

import com.common.android.view.ISupportFragView;
import com.moto.ezdriver.R;
import com.moto.ezdriver.view.activity.ActMain;
import com.moto.ezdriver.view.fragment.FragLogin;
import com.moto.ezdriver.view.fragment.FragSignUp;
import com.moto.ezdriver.view.holder.HolderFragSplash;

public class ControllerFragSplash implements View.OnClickListener {

    private final ISupportFragView mView;
    private HolderFragSplash mHolder;

    public ControllerFragSplash(final ISupportFragView view) {
        mView = view;
        mHolder = new HolderFragSplash(mView.getView());
        initializeActions();
    }

    private void initializeActions() {
        mHolder.getButtonLogin().setOnClickListener(this);
        mHolder.getButtonSignUp().setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        final int id = v.getId();
        if (id == R.id.frag_splash_bt_go_taxi) {
            onLogin();
        } else if (id == R.id.frag_splash_bt_sign_up) {
            onSignUp();
        }
    }

    private void onSignUp() {
        final int containerID = mView.getSupportView() instanceof ActMain ? R.id.act_main_container : R.id.act_auth_container;
        mView.getSupportView().navigateTo(containerID, new FragSignUp(), true);
    }

    private void onLogin() {
        final int containerID = mView.getSupportView() instanceof ActMain ? R.id.act_main_container : R.id.act_auth_container;
        mView.getSupportView().navigateTo(containerID, new FragLogin(), true);
    }
}
