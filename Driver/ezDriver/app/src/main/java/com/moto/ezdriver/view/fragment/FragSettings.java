package com.moto.ezdriver.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.common.android.view.CommonFragment;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.ControllerFragSettings;

public class FragSettings extends CommonFragment {

    private ControllerFragSettings mController;

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_settings, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable final Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            initializeActions();
        }
    }

    @Override
    public void initializeActions() {
        mController = new ControllerFragSettings(this);
    }

}
