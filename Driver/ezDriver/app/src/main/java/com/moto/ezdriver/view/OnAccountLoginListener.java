package com.moto.ezdriver.view;

import android.content.Intent;

public interface OnAccountLoginListener {
    void onAccountLoggedIn(final Intent intent);
}
