package com.moto.ezdriver.controller.net.conn.param;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.io.Serializable;

public class UpdatePositionParam implements Serializable {

    private String token;
    private double longitude;
    private double latitude;

    public UpdatePositionParam(final String token, final LatLng position) {
        this(token, position.longitude, position.latitude);
    }

    public UpdatePositionParam(final String token, final double longitude, final double latitude) {
        this.token = token;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
