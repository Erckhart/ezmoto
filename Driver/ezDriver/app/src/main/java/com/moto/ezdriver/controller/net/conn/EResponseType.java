package com.moto.ezdriver.controller.net.conn;

import android.content.Context;

import com.moto.ezdriver.R;

public enum EResponseType {

    I01(R.string.I01),

    E00(R.string.E00),
    E01(R.string.E01),
    E02(R.string.E02),
    E03(R.string.E03),
    E04(R.string.E04),
    E05(R.string.E05);

    private final int mMessageRes;

    EResponseType(final int messageRes) {
        mMessageRes = messageRes;
    }

    public int getMessage() {
        return mMessageRes;
    }

    public String getMessage(final Context context) {
        return context.getString(mMessageRes);
    }
}
