package com.moto.ezdriver.controller;

import com.common.android.view.ISupportView;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.net.Authenticator;
import com.moto.ezdriver.view.fragment.FragSettings;
import com.moto.ezdriver.view.fragment.FragSplash;

public class ControllerActMain {

    private final ISupportView mView;

    public ControllerActMain(final ISupportView view) {
        mView = view;
        initialize();
    }

    private void initialize() {
        if(Authenticator.isLoggedIn(mView.getBaseContext())) {
            mView.navigateTo(R.id.act_main_container, new FragSettings(), false);
        } else {
            mView.navigateTo(R.id.act_main_container, new FragSplash(), false);
        }
    }

}
