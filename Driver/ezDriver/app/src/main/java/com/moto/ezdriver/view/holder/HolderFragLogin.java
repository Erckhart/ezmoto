package com.moto.ezdriver.view.holder;

import android.graphics.PorterDuff;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.common.android.view.holder.HolderFrag;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.net.conn.param.LoginParam;
import com.moto.ezdriver.controller.util.RawUtil;

import me.zhanghai.android.materialprogressbar.IndeterminateHorizontalProgressDrawable;

public class HolderFragLogin extends HolderFrag {

    private EditText mLogin;
    private EditText mPass;

    private Button mEnter;
    private Button mForgotPass;

    private ProgressBar mLoading;

    public HolderFragLogin(final View view) {
        super(view);
    }

    @Override
    protected void initializeFields() {
        mLoading = (ProgressBar) getField(R.id.frag_login_loading);

        final IndeterminateHorizontalProgressDrawable drawable = new IndeterminateHorizontalProgressDrawable(getContext());
        drawable.setShowTrack(true);
        drawable.setUseIntrinsicPadding(true);
        drawable.setColorFilter(RawUtil.getColor(getContext(), R.color.red_dark), PorterDuff.Mode.SRC_IN);
        mLoading.setIndeterminateDrawable(drawable);

        mForgotPass = (Button) getField(R.id.frag_login_bt_forgot_pass);
        mEnter = (Button) getField(R.id.frag_login_bt_enter);
        mLogin = (EditText) getField(R.id.frag_login_et_user);
        mPass = (EditText) getField(R.id.frag_login_et_pass);
    }

    public LoginParam getLoginParam() {
        final String user = mLogin.getText().toString();
        final String pass = mPass.getText().toString();
        return new LoginParam(user, pass);
    }

    public EditText getEditTextPass() {
        return mPass;
    }

    public void setLoadingEnabled(final boolean enabled) {
        if (mLoading != null) {
            mLoading.setVisibility(enabled ? View.VISIBLE : View.GONE);
        }
    }

    public void setScreenFrozen(final boolean frozen) {
        mLogin.setEnabled(frozen);
        mPass.setEnabled(frozen);
        mEnter.setEnabled(frozen);
        mForgotPass.setEnabled(frozen);
    }

    public Button getButtonEnter() {
        return mEnter;
    }

}
