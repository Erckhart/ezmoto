package com.moto.ezdriver.controller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import com.common.android.os.BaseBroadcastReceiver;
import com.common.android.view.ISupportFragView;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.net.Authenticator;
import com.moto.ezdriver.controller.util.LocationUtils;
import com.moto.ezdriver.view.activity.ActMain;
import com.moto.ezdriver.view.dialog.DialogGPS;
import com.moto.ezdriver.view.fragment.FragSplash;
import com.moto.ezdriver.view.holder.HolderFragSettings;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;

public class ControllerFragSettings implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    /*
     * A constant representing 10 minutes in seconds.
     */
    private static final long FREQUENCY = 600;
    private static final int SYNCABLE = 1;
    private static final int UNSYNCABLE = 0;

    private static final String TAG = ControllerFragSettings.class.getName();

    private final ISupportFragView mView;
    private final HolderFragSettings mHolder;

    private final BaseBroadcastReceiver mGPSReceiver = new BaseBroadcastReceiver() {
        @Override
        protected IntentFilter getFilter() {
            return new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION);
        }

        @Override
        public void onReceive(final Context context, final Intent intent) {
            initializeFields();
        }
    };

    private DialogGPS mDialogGPS;

    public ControllerFragSettings(final ISupportFragView view) {
        mView = view;
        mHolder = new HolderFragSettings(view.getView());

        initializeActions();
        initializeFields();
    }

    private void initializeFields() {
        final boolean gpsEnabled = LocationUtils.isGpsEnabled(mView.getContext());
        if (gpsEnabled) {
            if (mDialogGPS != null) {
                mDialogGPS.dismiss();
                mDialogGPS = null;
            }
            final SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(mView.getContext());
            final boolean isAvailable = pref.getBoolean(mView.getString(R.string.pref_available_key), false);
            mHolder.getAvailableTrip().setChecked(isAvailable);
            final File file = new File(mView.getContext().getFilesDir(), "profile_image.png");
            Picasso.with(mView.getContext()).load(file).into(mHolder.getProfilePhoto());
        } else {
            mDialogGPS = getDialogGPS();
            mDialogGPS.show();
        }
        mGPSReceiver.setEnabled(mView.getContext(), !gpsEnabled);
    }

    private void initializeActions() {
        mHolder.getAvailableTrip().setOnCheckedChangeListener(this);
        mHolder.getLogoutButton().setOnClickListener(this);
    }

    private void setSyncEnabled(final boolean isDriverAvailable) {
        final Account account = Authenticator.getConnectedAccount(mView.getContext());
        if (account != null) {
            final String authority = mView.getString(R.string.app_account_authority);
            ContentResolver.setSyncAutomatically(account, authority, isDriverAvailable);
            ContentResolver.setIsSyncable(account, authority, isDriverAvailable ? SYNCABLE : UNSYNCABLE);
            if (isDriverAvailable) {
                ContentResolver.addPeriodicSync(account, authority, Bundle.EMPTY, FREQUENCY);
                ContentResolver.requestSync(account, authority, Bundle.EMPTY);
            } else {
                ContentResolver.removePeriodicSync(account, authority, Bundle.EMPTY);
            }
        }
    }

    private void onLogout() {
        try {
            final AccountManager manager = AccountManager.get(mView.getContext());
            final Account account = Authenticator.getConnectedAccount(mView.getContext());

            final Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        if (android.os.Build.VERSION.SDK_INT < 22) {
                            onLogoutBelowAPI22(manager, account);
                        } else {
                            onLogoutFromAPI22(manager, account);
                        }
                    } catch (AuthenticatorException e) {
                        e.printStackTrace();
                    } catch (OperationCanceledException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };

            final Thread t = new Thread(runnable);
            t.start();

        } catch (IllegalThreadStateException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("NewApi")
    private void onLogoutFromAPI22(final AccountManager manager, final Account account) throws AuthenticatorException, OperationCanceledException, IOException {
        final AccountManagerFuture<Bundle> bundleAccountManagerFuture = manager.removeAccount(account, null, null, null);
        final Bundle result = bundleAccountManagerFuture.getResult();
        if (result.getBoolean(AccountManager.KEY_BOOLEAN_RESULT, false)) {
            onLogoutFinish();
        }
    }

    private void onLogoutBelowAPI22(final AccountManager manager, final Account account) throws AuthenticatorException, OperationCanceledException, IOException {
        final AccountManagerFuture<Boolean> booleanAccountManagerFuture = manager.removeAccount(account, null, null);
        if (booleanAccountManagerFuture.getResult()) {
            onLogoutFinish();
        }
    }

    private void onLogoutFinish() {
        final SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(mView.getContext());
        defaultSharedPreferences.edit().clear().commit();

        final File file = new File(mView.getContext().getFilesDir(), "profile_image.png");
        final boolean deleted = file.delete();
        Log.e(TAG, "Profile photo deleted: " + deleted);

        final int containerID = mView.getSupportView() instanceof ActMain ? R.id.act_main_container : R.id.act_auth_container;
        mView.getSupportView().navigateTo(containerID, new FragSplash(), false);
    }

    @Override
    public void onCheckedChanged(final CompoundButton buttonView, final boolean isChecked) {
        final int id = buttonView.getId();
        if (id == R.id.frag_settings_sw_available_for_trip) {
            mHolder.getAvailableStatus().setText(isChecked ? R.string.pref_available_status_on : R.string.pref_available_status_off);
            setSyncEnabled(isChecked);
            final String key = mView.getString(R.string.pref_available_key);
            PreferenceManager.getDefaultSharedPreferences(mView.getContext()).edit().putBoolean(key, isChecked).commit();
        }
    }

    @Override
    public void onClick(final View v) {
        final int id = v.getId();
        if (id == R.id.frag_settings_bt_logout) {
            onLogout();
        }
    }

    public DialogGPS getDialogGPS() {
        return new DialogGPS(mView.getActivity());
    }
}
