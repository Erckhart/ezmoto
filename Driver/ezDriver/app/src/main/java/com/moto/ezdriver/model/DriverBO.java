package com.moto.ezdriver.model;

import java.io.Serializable;

public class DriverBO implements Serializable {

    private String name;
    private String email;
    private String phone;
    private String plate;
    private String regNumber;
    private String photo;
    private String token;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(final String plate) {
        this.plate = plate;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(final String regNumber) {
        this.regNumber = regNumber;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(final String photo) {
        this.photo = photo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }
}
