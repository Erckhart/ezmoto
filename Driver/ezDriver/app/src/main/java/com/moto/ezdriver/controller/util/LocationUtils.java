package com.moto.ezdriver.controller.util;

import android.content.Context;
import android.location.LocationManager;

public class LocationUtils {

    public static boolean isGpsEnabled(final Context context) {
        final LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

}
