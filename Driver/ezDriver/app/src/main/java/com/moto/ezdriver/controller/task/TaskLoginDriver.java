package com.moto.ezdriver.controller.task;

import android.content.Context;

import com.common.android.os.BaseAsyncTask;
import com.moto.ezdriver.controller.net.NetworkStatusException;
import com.moto.ezdriver.controller.net.conn.ConnLogin;
import com.moto.ezdriver.controller.net.conn.param.LoginParam;
import com.moto.ezdriver.model.DriverBO;

public class TaskLoginDriver extends BaseAsyncTask<LoginParam, Void, DriverBO> {

    private final OnLoginDriverListener mOnLoginDriverListener;

    private NetworkStatusException error;

    public TaskLoginDriver(final Context context, final OnLoginDriverListener listener) {
        super(context);
        mOnLoginDriverListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (mOnLoginDriverListener != null) {
            mOnLoginDriverListener.onLoginStart();
        }
    }

    @Override
    protected DriverBO doInBackground(final LoginParam... params) {
        DriverBO driver = null;
        try {
            final LoginParam param = params[0];
            final ConnLogin connection = new ConnLogin(param);
            connection.execute();
            driver = connection.getResult();
            driver.setEmail(param.getEmail());
        } catch (NetworkStatusException e) {
            error = e;
            e.printStackTrace();
        }
        return driver;
    }

    @Override
    protected void onPostExecute(final DriverBO driverBO) {
        super.onPostExecute(driverBO);
        if (!isCancelled() && mOnLoginDriverListener != null) {
            if (driverBO == null) {
                mOnLoginDriverListener.onLoginError(error);
            } else {
                mOnLoginDriverListener.onLoggedIn(driverBO);
            }
        }
    }

    public interface OnLoginDriverListener {
        void onLoginStart();

        void onLoggedIn(final DriverBO driver);

        void onLoginError(final NetworkStatusException e);
    }

}
