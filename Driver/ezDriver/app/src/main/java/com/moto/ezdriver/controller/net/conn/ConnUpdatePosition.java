package com.moto.ezdriver.controller.net.conn;

import com.moto.ezdriver.controller.net.NetworkStatusException;
import com.moto.ezdriver.controller.net.conn.param.UpdatePositionParam;

import java.net.MalformedURLException;

public class ConnUpdatePosition extends BaseConn {

    private final UpdatePositionParam mParam;

    public ConnUpdatePosition(final UpdatePositionParam param) {
        mParam = param;
    }

    @Override
    public EConnectionType getCommand() throws MalformedURLException {
        return EConnectionType.DRIVER_UPDATE_POSITION;
    }

    @Override
    public void execute() throws NetworkStatusException {
        connect(METHOD_POST, MEDIUM_TIMEOUT, mParam.toString());
    }

    @Override
    public Integer getResult() throws NetworkStatusException {
        return null;
    }
}
