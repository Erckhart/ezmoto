package com.moto.ezdriver;

import com.common.android.app.CommonApplication;

import java.util.Objects;

public class MotoApp extends CommonApplication {

    private final Object _lock = new Object();

    private static MotoApp _instance;

    public static synchronized MotoApp getInstance() {
        return _instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        synchronized (_lock) {
            _instance = this;
        }
    }
}
