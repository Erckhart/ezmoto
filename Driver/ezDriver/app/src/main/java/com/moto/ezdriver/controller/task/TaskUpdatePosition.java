/*
 * Copyright (C) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Mobile Communication Division,
 * Digital Media & Communications Business, Samsung Electronics Co., Ltd.
 *
 * This software and its documentation are confidential and proprietary
 * information of Samsung Electronics Co., Ltd.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to
 * any electronic medium or machine-readable form without the prior written
 * consent of Samsung Electronics.
 *
 * Samsung Electronics makes no representations with respect to the contents,
 * and assumes no responsibility for any errors that might appear in the
 * software and documents. This publication and the contents hereof are subject
 * to change without notice.
 */
package com.moto.ezdriver.controller.task;

import android.content.Context;
import android.location.Location;

import com.common.android.os.BaseAsyncTask;
import com.moto.ezdriver.controller.net.NetworkStatusException;
import com.moto.ezdriver.controller.net.conn.ConnUpdatePosition;
import com.moto.ezdriver.controller.net.conn.param.UpdatePositionParam;

public class TaskUpdatePosition extends BaseAsyncTask<Location,Void,Void> {

    private final String mToken;

    public TaskUpdatePosition(final String token, final Context context) {
        super(context);
        mToken = token;
    }

    @Override
    protected Void doInBackground(final Location... params) {
        try {
            final Location location = params[0];
            final ConnUpdatePosition connection = new ConnUpdatePosition(new UpdatePositionParam(mToken, location.getLongitude(), location.getLatitude()));
            connection.execute();
        } catch (NetworkStatusException e) {
            e.printStackTrace();
        }
        return null;
    }
}
