package com.moto.ezdriver.controller.net.conn.param;

import java.io.Serializable;

abstract class BaseParam implements Serializable {

    protected abstract String getAsJson();

    @Override
    public String toString() {
        return getAsJson();
    }
}
