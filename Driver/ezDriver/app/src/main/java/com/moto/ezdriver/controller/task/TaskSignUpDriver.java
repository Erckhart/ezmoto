package com.moto.ezdriver.controller.task;

import android.content.Context;

import com.common.android.os.BaseAsyncTask;
import com.moto.ezdriver.controller.net.NetworkStatusException;
import com.moto.ezdriver.controller.net.conn.ConnSignUp;
import com.moto.ezdriver.controller.net.conn.param.SignUpParam;
import com.moto.ezdriver.controller.util.RawUtil;
import com.moto.ezdriver.model.DriverBO;

public class TaskSignUpDriver extends BaseAsyncTask<SignUpParam, Void, DriverBO> {

    private final OnSignUpDriverListener mOnSignUpDriverListener;

    private NetworkStatusException mNetworkStatusException;

    public TaskSignUpDriver(final Context context, final OnSignUpDriverListener onSignUpDriverListener) {
        super(context);
        mOnSignUpDriverListener = onSignUpDriverListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(mOnSignUpDriverListener != null) {
            mOnSignUpDriverListener.onSignUpStart();
        }
    }

    @Override
    protected DriverBO doInBackground(final SignUpParam... params) {
        DriverBO driver = null;
        try {
            final SignUpParam param = params[0];
            final ConnSignUp connection = new ConnSignUp(param);
            connection.execute();
            driver = connection.getResult();
            if(!RawUtil.isEmptyNullOrWhitespace(driver.getToken())) {
                driver.setEmail(param.getUser().getEmail());
                driver.setName(param.getUser().getName());
            }
        } catch (NetworkStatusException e) {
            mNetworkStatusException = e;
        }
        return driver;
    }

    @Override
    protected void onPostExecute(final DriverBO driver) {
        super.onPostExecute(driver);
        if (!isCancelled() && mOnSignUpDriverListener != null) {
            if (driver == null) {
                mOnSignUpDriverListener.onSignUpError(mNetworkStatusException);
            } else {
                mOnSignUpDriverListener.onSignedUp(driver);
            }
        }
    }

    public interface OnSignUpDriverListener {
        void onSignUpStart();

        void onSignUpError(final NetworkStatusException e);

        void onSignedUp(final DriverBO driver);
    }

}
