package com.moto.ezdriver.controller.net.conn;

import com.google.gson.Gson;
import com.moto.ezdriver.controller.net.NetworkStatusException;
import com.moto.ezdriver.controller.net.conn.param.SignUpParam;
import com.moto.ezdriver.model.DriverBO;

import java.net.MalformedURLException;

public class ConnSignUp extends BaseConn {

    private final SignUpParam mParam;

    public ConnSignUp(final SignUpParam param) {
        mParam = param;
    }

    @Override
    public EConnectionType getCommand() throws MalformedURLException {
        return EConnectionType.DRIVER_SIGN_UP;
    }

    @Override
    public void execute() throws NetworkStatusException {
        connect(METHOD_POST, MEDIUM_TIMEOUT, mParam.toString());
    }

    @Override
    public DriverBO getResult() throws NetworkStatusException {
        return new Gson().fromJson(getData(), DriverBO.class);
    }
}
