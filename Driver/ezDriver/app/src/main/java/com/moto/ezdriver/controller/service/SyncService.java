package com.moto.ezdriver.controller.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.moto.ezdriver.controller.net.SyncAdapter;

public class SyncService extends Service {

    private static final Object sSyncAdapterLock = new Object();

    private SyncAdapter mSyncAdapter;

    @Override
    public void onCreate() {
        super.onCreate();
        synchronized (sSyncAdapterLock) {
            if(mSyncAdapter == null) {
                mSyncAdapter = new SyncAdapter(this, true);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(final Intent intent) {
        return mSyncAdapter.getSyncAdapterBinder();
    }

}
