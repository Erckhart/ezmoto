package com.moto.ezdriver.view.activity;

import android.content.Intent;
import android.os.Bundle;

import com.common.android.view.CommonActivity;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.ControllerActMain;

public class ActMain extends CommonActivity {

    private ControllerActMain mController;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        initializeActions();
    }

    @Override
    public void initializeActions() {
        mController = new ControllerActMain(this);
    }

}
