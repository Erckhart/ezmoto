package com.moto.ezdriver.controller.net.conn.param;

import com.google.gson.Gson;
import com.moto.ezdriver.controller.util.RawUtil;

import java.io.Serializable;

public class LoginParam implements Serializable {

    private final String email;
    private final String password;

    public LoginParam(final String user, final String pass) {
        email = user;
        password = RawUtil.toMD5(pass);
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public boolean isRequiredInfoNull() {
        return RawUtil.isEmptyNullOrWhitespace(email) || RawUtil.isEmptyNullOrWhitespace(password);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}
