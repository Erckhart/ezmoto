package com.moto.ezdriver.controller.net;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.Context;
import android.content.SyncResult;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.moto.ezdriver.controller.task.TaskUpdatePosition;

public class SyncAdapter extends AbstractThreadedSyncAdapter implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final AccountManager mAccountManager;
    private final GoogleApiClient mGoogleApiClient;

    private String mToken;

    public SyncAdapter(final Context context, final boolean autoInitialize) {
        super(context, autoInitialize);
        mAccountManager = AccountManager.get(context);
        mGoogleApiClient = getGoogleClient();
    }

    @Override
    public void onPerformSync(final Account account, final Bundle extras, final String authority, final ContentProviderClient provider, final SyncResult syncResult) {
        try {
            mToken = mAccountManager.blockingGetAuthToken(account, Authenticator.ACCOUNT_TYPE, true);
            if (TextUtils.isEmpty(mToken)) {
                //TODO unregister this adapter
            } else {
                mGoogleApiClient.connect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private GoogleApiClient getGoogleClient() {
        return new GoogleApiClient.Builder(getContext())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
    }

    @Override
    public void onConnected(final Bundle bundle) {
        final Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(location != null) {
            sendNewLocation(location);
        }
    }

    private void sendNewLocation(final Location location) {
        new TaskUpdatePosition(mToken, getContext()).execute(location);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionSuspended(final int reason) {

    }

    @Override
    public void onConnectionFailed(final ConnectionResult result) {

    }
}
