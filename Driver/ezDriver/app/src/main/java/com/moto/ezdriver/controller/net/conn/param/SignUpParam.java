package com.moto.ezdriver.controller.net.conn.param;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.moto.ezdriver.controller.util.RawUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class SignUpParam extends BaseParam {

    private String token;
    private UserParam user;

    public SignUpParam(final UserParam userParam) {
        user = userParam;
    }

    public UserParam getUser() {
        return user;
    }

    @Override
    protected String getAsJson() {
        final String token = RawUtil.toMD5(user.getEmail() + "-" + RawUtil.encode(user.getName()));
        this.token = token;
        final String json = new Gson().toJson(this);
        return json;
    }
}
