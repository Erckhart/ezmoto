package com.moto.ezdriver.view.holder;

import android.view.View;
import android.widget.Button;

import com.common.android.view.holder.HolderFrag;
import com.moto.ezdriver.R;

public class HolderFragSplash extends HolderFrag {

    private Button mLogin;
    private Button mSignUp;

    public HolderFragSplash(final View view) {
        super(view);
    }

    @Override
    protected void initializeFields() {
        mLogin = (Button) getField(R.id.frag_splash_bt_go_taxi);
        mSignUp = (Button) getField(R.id.frag_splash_bt_sign_up);
    }

    public Button getButtonLogin() {
        return mLogin;
    }

    public Button getButtonSignUp() {
        return mSignUp;
    }
}
