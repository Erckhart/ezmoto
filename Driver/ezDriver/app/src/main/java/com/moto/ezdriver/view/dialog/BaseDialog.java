package com.moto.ezdriver.view.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;

import com.moto.ezdriver.R;

class BaseDialog {
    protected AlertDialog.Builder builder;
    protected AlertDialog mDialog = null;
    protected View mView;
    protected Context mContext;

    public BaseDialog(final Context context, final int layout) {
        mContext = context;
        configure(layout);
    }

    private void configure(final int layout) {
        builder = new AlertDialog.Builder(mContext);
        builder.setCancelable(true);

        final LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = inflater.inflate(layout, null);
        builder.setView(mView);
    }

    public Context getContext() {
        return mContext;
    }

    public View getView() {
        return mView;
    }

    public void show() {
        mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.show();
        mDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void dismiss() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog.dismiss();
        }
    }
}
