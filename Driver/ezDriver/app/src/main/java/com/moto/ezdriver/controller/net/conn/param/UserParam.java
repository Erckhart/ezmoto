package com.moto.ezdriver.controller.net.conn.param;

import com.google.gson.Gson;
import com.moto.ezdriver.controller.util.RawUtil;

public class UserParam extends BaseParam {

    private String name;
    private String password;
    private String email;
    private int status = 1; // pending
    private final int user_type = 2; //driver

    public UserParam(final String name, final String pass, final String email) {
        this.name = name;
        this.email = email;
        setPass(pass);
    }

    private void setPass(final String pass) {
        this.password = RawUtil.toMD5(pass);
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPass() {
        return password;
    }

    @Override
    protected String getAsJson() {
        return new Gson().toJson(this);
    }
}
