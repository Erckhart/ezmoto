package com.moto.ezdriver.view.dialog;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.view.View;

import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.util.LocationUtils;

public class DialogGPS extends BaseDialog {

    private final BroadcastReceiver mGPSReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(final Context context, final Intent intent) {
            if(LocationUtils.isGpsEnabled(context)) {
                dismiss();
            }
        }
    };

    private View.OnClickListener mOkClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {

        }
    };

    public DialogGPS(final Context context) {
        super(context, R.layout.dialog_gps);
        initializeFields();
        initialize();
    }

    private void initialize() {
        final boolean gpsEnabled = LocationUtils.isGpsEnabled(getContext());
        if (!gpsEnabled) {
            show();
        }
    }

    private void initializeFields() {
        getView().findViewById(R.id.ezdriver_bt_ok).setOnClickListener(mOkClickListener);
    }

    @Override
    public void show() {
        getContext().registerReceiver(mGPSReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        super.show();
    }

    @Override
    public void dismiss() {
        getContext().unregisterReceiver(mGPSReceiver);
        super.dismiss();
    }
}
