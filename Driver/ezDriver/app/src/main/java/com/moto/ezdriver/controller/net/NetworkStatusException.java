package com.moto.ezdriver.controller.net;

import com.moto.ezdriver.MotoApp;
import com.moto.ezdriver.controller.net.conn.EResponseType;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public class NetworkStatusException extends IOException {

    public static final String CODE_KEY = "code";

    private String mCode;

    public NetworkStatusException(final Throwable cause) {
        super(cause);
        mCode = EResponseType.E00.toString();
    }

    public NetworkStatusException(final String json) {
        try {
            final JSONObject jsonObject = new JSONObject(json);
            mCode = jsonObject.getString(CODE_KEY);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public EResponseType getResponseType() {
        EResponseType type = null;
        try {
            type = EResponseType.valueOf(mCode);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return type;
    }

    public String getCode() {
        return mCode;
    }

    @Override
    public String getMessage() {
        final EResponseType type = getResponseType();
        if(type == null) {
            return super.getMessage();
        } else {
            return type.getMessage(MotoApp.getInstance().getApplicationContext());
        }
    }
}
