package com.moto.ezdriver.view.dialog;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.moto.ezdriver.R;

public class DialogWarning extends BaseDialog {

    private final TextView mTitleView;
    private final TextView mSummaryView;
    private final Button mOKView;

    private final View.OnClickListener mOKClickListener = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            dismiss();
        }
    };

    public DialogWarning(final Context context) {
        super(context, R.layout.dialog_warning);
        mTitleView = (TextView) getView().findViewById(R.id.dialog_warning_title);
        setTitle(R.string.warning_title_default);
        mSummaryView = (TextView) getView().findViewById(R.id.dialog_warning_summary);
        mOKView = (Button) getView().findViewById(R.id.dialog_warning_confirm);
        mOKView.setOnClickListener(mOKClickListener);
    }

    public void setTitle(final int title) {
        mTitleView.setText(title);
    }

    public void setTitle(final String title) {
        mTitleView.setText(title);
    }

    public void setSummary(final int summary) {
        mSummaryView.setText(summary);
    }

    public void setSummary(final String summary) {
        mSummaryView.setText(summary);
    }

}
