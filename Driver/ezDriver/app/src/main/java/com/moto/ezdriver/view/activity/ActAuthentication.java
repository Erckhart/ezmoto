package com.moto.ezdriver.view.activity;

import android.content.Intent;
import android.os.Bundle;

import com.common.android.view.ISupportView;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.ControllerActAuthentication;
import com.moto.ezdriver.view.OnAccountLoginListener;
import com.moto.ezdriver.view.widget.AccountAuthenticatorActivity;

public class ActAuthentication extends AccountAuthenticatorActivity implements OnAccountLoginListener {

    public static String ARG_IS_ADDING_NEW_ACCOUNT = "ARANW";
    public static String ARG_ACCOUNT_NAME = "AAN";

    private ControllerActAuthentication mController;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_authentication);
        initializeActions();
    }

    @Override
    public void initializeActions() {
        mController = new ControllerActAuthentication(this);
    }

    @Override
    public void onAccountLoggedIn(final Intent intent) {
        setAccountAuthenticatorResult(intent.getExtras());
    }

}
