package com.moto.ezdriver.controller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.common.android.view.ISupportFragView;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.net.Authenticator;
import com.moto.ezdriver.controller.net.NetworkStatusException;
import com.moto.ezdriver.controller.net.conn.ConnLogin;
import com.moto.ezdriver.controller.net.conn.EResponseType;
import com.moto.ezdriver.controller.net.conn.param.LoginParam;
import com.moto.ezdriver.controller.task.TaskLoginDriver;
import com.moto.ezdriver.controller.util.RawUtil;
import com.moto.ezdriver.model.DriverBO;
import com.moto.ezdriver.view.OnAccountLoginListener;
import com.moto.ezdriver.view.activity.ActAuthentication;
import com.moto.ezdriver.view.activity.ActMain;
import com.moto.ezdriver.view.dialog.DialogWarning;
import com.moto.ezdriver.view.fragment.FragSettings;
import com.moto.ezdriver.view.holder.HolderFragLogin;

public class ControllerFragLogin implements View.OnClickListener, TaskLoginDriver.OnLoginDriverListener, TextView.OnEditorActionListener {

    private final ISupportFragView mView;
    private final HolderFragLogin mHolder;
    private TaskLoginDriver mTask;

    private DialogWarning mDialogWarning;

    public ControllerFragLogin(final ISupportFragView view) {
        mView = view;
        mHolder = new HolderFragLogin(view.getView());
        initializeActions();
    }

    private void initializeActions() {
        mHolder.getButtonEnter().setOnClickListener(this);
        mHolder.getEditTextPass().setOnEditorActionListener(this);
    }

    @Override
    public void onClick(final View v) {
        final int id = v.getId();
        if (id == R.id.frag_login_bt_enter) {
            onLogin(mHolder.getLoginParam());
        }
    }

    private void onLogin(final LoginParam param) {
        if (param != null) {
            cancel();
            if(param.isRequiredInfoNull()) {
                showDialogWaning(EResponseType.E04.getMessage(mView.getContext()));
            } else {
                mTask = new TaskLoginDriver(mView.getSupportView().getBaseContext(), this);
                mTask.execute(param);
            }
        }
    }

    public void cancel() {
        if (mTask != null) {
            mTask.cancel(true);
        }
    }

    @Override
    public void onLoginStart() {
        mHolder.setLoadingEnabled(true);
        mHolder.setScreenFrozen(true);
    }

    @Override
    public void onLoggedIn(final DriverBO driver) {
        mHolder.setLoadingEnabled(false);
        mHolder.setScreenFrozen(false);
        final Account account = new Account(driver.getEmail(), Authenticator.ACCOUNT_TYPE);
        final AccountManager am = AccountManager.get(mView.getSupportView().getBaseContext());
        am.addAccountExplicitly(account, mHolder.getLoginParam().getPassword(), null);
        am.setAuthToken(account, Authenticator.ACCOUNT_TYPE, driver.getToken());

        mView.getSupportView().clearNavigation();

        if(mView.getSupportView() instanceof ActAuthentication) {
            final Intent intent = new Intent();
            intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, driver.getEmail());
            intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Authenticator.ACCOUNT_TYPE);
            intent.putExtra(AccountManager.KEY_AUTHTOKEN, driver.getToken());
            ((OnAccountLoginListener)mView.getSupportView()).onAccountLoggedIn(intent);
            mView.getSupportView().navigateTo(ActMain.class);
            mView.getSupportView().finish();
        } else {
            mView.getSupportView().navigateTo(R.id.act_main_container, new FragSettings(), false);
        }
    }

    @Override
    public void onLoginError(final NetworkStatusException error) {
        mHolder.setLoadingEnabled(false);
        mHolder.setScreenFrozen(false);

        showDialogWaning(error.getMessage());
    }

    private void showDialogWaning(final String warning) {
        if(!RawUtil.isEmptyNullOrWhitespace(warning)) {
            if (mDialogWarning != null) {
                mDialogWarning.dismiss();
            }
            mDialogWarning = new DialogWarning(mView.getContext());
            mDialogWarning.setSummary(warning);
            mDialogWarning.show();
        }
    }

    @Override
    public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
        onLogin(mHolder.getLoginParam());
        return false;
    }

    public boolean onBackPressed() {
        boolean handled = false;
        if(mTask != null && mTask.getStatus() == AsyncTask.Status.RUNNING) {
            cancel();
        }
        return handled;
    }
}
