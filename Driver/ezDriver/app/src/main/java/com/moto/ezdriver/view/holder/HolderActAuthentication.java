package com.moto.ezdriver.view.holder;

import android.widget.FrameLayout;

import com.common.android.view.ISupportView;
import com.common.android.view.holder.HolderAct;
import com.moto.ezdriver.R;

public class HolderActAuthentication extends HolderAct {

    private FrameLayout mFragmentContainer;

    public HolderActAuthentication(final ISupportView view) {
        super(view);
    }

    @Override
    protected void initializeFields() {
        mFragmentContainer = (FrameLayout) getField(R.id.act_auth_container);
    }

    public FrameLayout getFragmentContainer() {
        return mFragmentContainer;
    }
}
