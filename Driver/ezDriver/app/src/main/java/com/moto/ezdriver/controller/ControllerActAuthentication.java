package com.moto.ezdriver.controller;

import android.content.Intent;

import com.common.android.view.ISupportView;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.net.Authenticator;
import com.moto.ezdriver.view.activity.ActMain;
import com.moto.ezdriver.view.fragment.FragSplash;
import com.moto.ezdriver.view.holder.HolderActAuthentication;

public class ControllerActAuthentication {

    private HolderActAuthentication mHolder;
    private final ISupportView mView;

    public ControllerActAuthentication(final ISupportView view) {
        mView = view;
        mHolder = new HolderActAuthentication(mView);
        initialize();
    }

    private void initialize() {
        if(Authenticator.isLoggedIn(mView.getBaseContext())) {
            mView.startActivity(new Intent(mView.getBaseContext(), ActMain.class));
        } else {
            mView.navigateTo(R.id.act_auth_container, new FragSplash(), false);
        }
    }

}
