package com.moto.ezdriver.controller.net.conn;

import com.moto.ezdriver.controller.net.NetworkStatusException;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

abstract class BaseConn {

    private HttpURLConnection con;
    private static final String LINE = "\r\n";
    protected static final String METHOD_GET = "GET";
    protected static final String METHOD_POST = "POST";
    protected static final int MEDIUM_TIMEOUT = 5000;
    protected static final int MAX_TIMEOUT = 10000;
    public static final String UTF_8 = "UTF-8";

    /**
     * connection on server by URL
     *
     * @param method  post or get
     * @param timeout for connection
     * @param data    the json post
     * @throws NetworkStatusException when any exception was raised
     */
    protected void connect(final String method, final int timeout,
                           final String data) throws NetworkStatusException {
        try {
            final URL url = new URL(getCommand().getUrl());
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(method);
            con.setRequestProperty("content-type", "application/json;  charset=utf-8");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setConnectTimeout(timeout);
            if (method.equals(METHOD_POST))
                this.setData(data);
            con.connect();
        } catch (IOException e) {
            throw new NetworkStatusException(e);
        }
    }

    private void setData(final String rawData) throws IOException {
        if (con != null) {
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.write(rawData.getBytes(UTF_8));
            wr.flush();
            wr.close();
        }
    }

    /**
     * Return data of server InputStream
     *
     * @return {@link String}
     * @throws NetworkStatusException
     */
    protected String getData() throws NetworkStatusException {
        String result = "";
        try {
            InputStream stream = null;
            if (con.getErrorStream() != null) {
                stream = con.getErrorStream();
            } else {
                stream = con.getInputStream();
            }
            result = readStream(stream);
        } catch(IOException e) {
            throw  new NetworkStatusException(e);
        } finally {
            con.disconnect();
        }
        if(con.getErrorStream() != null) {
            throw new NetworkStatusException(result);
        }
        return result;
    }

    private String readStream(final InputStream stream) throws IOException {
        try {
            final StringBuilder buffer = new StringBuilder();
            BufferedReader mBr = new BufferedReader(new InputStreamReader(stream, UTF_8));
            String line = null;
            while ((line = mBr.readLine()) != null) {
                buffer.append(line + LINE);
            }
            return buffer.toString();
        } finally {
            stream.close();
        }
    }

    /**
     * abstract method for create URL for connect to server
     *
     * @throws MalformedURLException
     */
    public abstract EConnectionType getCommand() throws MalformedURLException;

    public abstract void execute() throws NetworkStatusException;

    public abstract Object getResult() throws NetworkStatusException;

}
