package com.moto.ezdriver.controller.net;

import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.moto.ezdriver.view.activity.ActAuthentication;

public class Authenticator extends AbstractAccountAuthenticator {

    public static final String ACCOUNT_TYPE = "com.moto.ezdriver";

    private final Context mContext;

    public Authenticator(final Context context) {
        super(context);
        mContext = context;
    }

    @Override
    public Bundle editProperties(final AccountAuthenticatorResponse response, final String accountType) {
        return null;
    }

    @Override
    public Bundle addAccount(final AccountAuthenticatorResponse response, final String accountType, final String authTokenType, final String[] requiredFeatures, final Bundle options) throws NetworkErrorException {
        final Bundle result = new Bundle();
        if(isLoggedIn(mContext)) {
            Toast.makeText(mContext,"Somente uma conta por vez",Toast.LENGTH_SHORT).show();
        } else {
            final Intent addAccountIntent = new Intent(mContext, ActAuthentication.class);
            addAccountIntent.putExtra(ACCOUNT_TYPE, accountType);
            addAccountIntent.putExtra(ActAuthentication.ARG_IS_ADDING_NEW_ACCOUNT, true);
            addAccountIntent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
            result.putParcelable(AccountManager.KEY_INTENT, addAccountIntent);
        }
        return result;
    }

    @Override
    public Bundle confirmCredentials(final AccountAuthenticatorResponse response, final Account account, final Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle getAuthToken(final AccountAuthenticatorResponse response, final Account account, final String authTokenType, final Bundle options) throws NetworkErrorException {
        final AccountManager manager = AccountManager.get(mContext);
        final String authToken = manager.peekAuthToken(account, authTokenType);

        if (!TextUtils.isEmpty(authToken)) {
            final Bundle result = new Bundle();
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken);
            return result;
        }

        final Intent intent = new Intent(mContext, ActAuthentication.class);

        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
        intent.putExtra(ActAuthentication.ARG_IS_ADDING_NEW_ACCOUNT, false);
        intent.putExtra(ActAuthentication.ARG_ACCOUNT_NAME, account.name);
//        intent.putExtra(LoginActivity.ARG_ACCOUNT_TYPE, account.type);
//        intent.putExtra(LoginActivity.ARG_AUTH_TYPE, authTokenType);

        final Bundle bundle = new Bundle();
        bundle.putParcelable(AccountManager.KEY_INTENT, intent);
        return bundle;
    }

    @Override
    public Bundle getAccountRemovalAllowed(final AccountAuthenticatorResponse response, final Account account) throws NetworkErrorException {
        final Bundle result = new Bundle();
        result.putBoolean(AccountManager.KEY_BOOLEAN_RESULT, true);
        return result;
    }

    @Override
    public String getAuthTokenLabel(final String authTokenType) {
        return null;
    }

    @Override
    public Bundle updateCredentials(final AccountAuthenticatorResponse response, final Account account, final String authTokenType, final Bundle options) throws NetworkErrorException {
        return null;
    }

    @Override
    public Bundle hasFeatures(final AccountAuthenticatorResponse response, final Account account, final String[] features) throws NetworkErrorException {
        return null;
    }

    public static boolean isLoggedIn(final Context context) {
        final AccountManager am = AccountManager.get(context);
        final Account[] accounts = am.getAccountsByType(ACCOUNT_TYPE);
        return accounts.length > 0;
    }

    public static Account getConnectedAccount(final Context context) {
        Account account = null;
        if(isLoggedIn(context)) {
            return AccountManager.get(context).getAccountsByType(ACCOUNT_TYPE)[0];
        }
        return account;
    }

}
