package com.moto.ezdriver.controller;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.common.android.view.ISupportFragView;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.net.Authenticator;
import com.moto.ezdriver.controller.net.NetworkStatusException;
import com.moto.ezdriver.controller.net.conn.param.SignUpParam;
import com.moto.ezdriver.controller.task.TaskSignUpDriver;
import com.moto.ezdriver.model.DriverBO;
import com.moto.ezdriver.view.OnAccountLoginListener;
import com.moto.ezdriver.view.activity.ActAuthentication;
import com.moto.ezdriver.view.activity.ActMain;
import com.moto.ezdriver.view.dialog.DialogWarning;
import com.moto.ezdriver.view.fragment.FragSettings;
import com.moto.ezdriver.view.holder.HolderFragSignUp;

public class ControllerFragSignUp implements View.OnClickListener, TaskSignUpDriver.OnSignUpDriverListener, TextView.OnEditorActionListener {

    //private static final int PICK_PHOTO_FOR_AVATAR = 12345;
    //private static final int CROP_IMAGE = 4;

    private HolderFragSignUp mHolder;
    private ISupportFragView mView;

    private TaskSignUpDriver mTask;

    private DialogWarning mDialogWarning;

    public ControllerFragSignUp(final ISupportFragView view) {
        mView = view;
        mHolder = new HolderFragSignUp(mView.getView());
        initializeActions();
    }

    private void initializeActions() {
        mHolder.getPassword().setOnEditorActionListener(this);
        mHolder.getButtonCancel().setOnClickListener(this);
        mHolder.getButtonNext().setOnClickListener(this);
    }

    @Override
    public void onClick(final View v) {
        final int id = v.getId();
        if (id == R.id.frag_sign_up_bt_cancel) {
            onCancel();
        } else if (id == R.id.frag_sign_up_bt_next) {
            onNext();
        }
    }

//    private void onProfilePhoto() {
//
//        final Intent mGallery = new Intent(Intent.ACTION_GET_CONTENT, null);
//        mGallery.setType("image/*");
//        mGallery.addCategory(Intent.CATEGORY_OPENABLE);
//
//        final Intent mCamera = new Intent(
//                android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//        final Intent chooser = new Intent(Intent.ACTION_CHOOSER);
//        chooser.putExtra(Intent.EXTRA_INTENT, mGallery);
//        chooser.putExtra(Intent.EXTRA_TITLE, mView.getString(R.string.lbl_profile_photo));
//
//        final Intent[] intentArray = {mCamera};
//        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
//
//        mView.startActivityForResult(chooser, PICK_PHOTO_FOR_AVATAR);
//    }

    public void cancel() {
        if (mTask != null) {
            mTask.cancel(true);
        }
    }

    private void onNext() {
        final SignUpParam params = mHolder.getSignUpInfo();
        if (areAllFieldsValid(params)) {
            cancel();
            mTask = new TaskSignUpDriver(mView.getContext(), this);
            mTask.execute(params);
        } else {
            Toast.makeText(mView.getContext(), "PANNN", Toast.LENGTH_SHORT).show();
        }
    }

    private void onCancel() {
        cancel();
        mView.getSupportView().onBackPressed();
    }

//    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
//        if (resultCode == Activity.RESULT_OK) {
//            if (requestCode == PICK_PHOTO_FOR_AVATAR) {
//                handlePickImage(data);
//            } else if (requestCode == CROP_IMAGE) {
//                handleCropImage();
//            }
//        }
//    }
//
//    private void handleCropImage() {
//        mHolder.getButtonProfilePhoto().setImageBitmap(null);
//        final File file = new File(mView.getContext().getFilesDir(), "profile_image.png");
//        Picasso.with(mView.getContext())
//                .load(file)
//                .resize(mHolder.getButtonProfilePhoto().getWidth(), mHolder.getButtonProfilePhoto().getHeight())
//                .centerInside()
//                .into(mHolder.getButtonProfilePhoto());
//    }
//
//    private void handlePickImage(final Intent data) {
//        if (data == null) {
//            onProfilePhotoError();
//        } else {
//            onProfilePhotoSuccess(data);
//        }
//    }
//
//    private void onProfilePhotoError() {
//
//    }
//
//    private void onProfilePhotoSuccess(final Intent data) {
//        final Uri saveUri = Uri.fromFile(new File(mView.getContext().getFilesDir(), "profile_image.png"));
//        final CropImageIntentBuilder builder = new CropImageIntentBuilder(256, 256, saveUri);
//        builder.setSourceImage(data.getData());
//
//        final Intent intent = builder.getIntent(mView.getContext());
//        intent.putExtra("circleCrop", "true");
//        intent.putExtra("outputFormat","PNG");
//
//        mView.startActivityForResult(intent, CROP_IMAGE);
//
//    }

    @Override
    public void onSignUpStart() {
        mHolder.setLoadingEnabled(true);
    }

    @Override
    public void onSignUpError(final NetworkStatusException e) {
        mHolder.setLoadingEnabled(false);
        if(mDialogWarning != null) {
            mDialogWarning.dismiss();
        }
        mDialogWarning = new DialogWarning(mView.getContext());
        mDialogWarning.setSummary(e.getMessage());
        mDialogWarning.show();
    }

    @Override
    public void onSignedUp(final DriverBO driver) {
        mHolder.setLoadingEnabled(false);
        final Account account = new Account(driver.getEmail(), Authenticator.ACCOUNT_TYPE);
        final AccountManager am = AccountManager.get(mView.getSupportView().getBaseContext());
        am.addAccountExplicitly(account, null, null);
        am.setAuthToken(account, Authenticator.ACCOUNT_TYPE, driver.getToken());

        mView.getSupportView().clearNavigation();

        if(mView.getSupportView() instanceof ActAuthentication) {
            final Intent intent = new Intent();
            intent.putExtra(AccountManager.KEY_ACCOUNT_NAME, driver.getEmail());
            intent.putExtra(AccountManager.KEY_ACCOUNT_TYPE, Authenticator.ACCOUNT_TYPE);
            intent.putExtra(AccountManager.KEY_AUTHTOKEN, driver.getToken());
            ((OnAccountLoginListener)mView.getSupportView()).onAccountLoggedIn(intent);
            mView.getSupportView().navigateTo(ActMain.class);
            mView.getSupportView().finish();
        } else {
            mView.getSupportView().navigateTo(R.id.act_main_container, new FragSettings(), false);
        }
    }

    private boolean areAllFieldsValid(final SignUpParam param) {
        boolean areAllValid = true;
        if (isEmpty(param.getUser().getName())) {
            areAllValid = false;
        }
        if (isEmpty(param.getUser().getEmail())) {
            areAllValid = false;
        }
        if (isEmpty(param.getUser().getPass())) {
            areAllValid = false;
        }
        return areAllValid;
    }

    private boolean isEmpty(final String text) {
        return TextUtils.isEmpty(text) || text.trim().length() == 0;
    }

    @Override
    public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
        final int id = v.getId();
        if(id == mHolder.getPassword().getId() && actionId == EditorInfo.IME_ACTION_DONE) {
            onNext();
            return true;
        }
        return false;
    }

    public void onBackPressed() {
        cancel();
    }
}
