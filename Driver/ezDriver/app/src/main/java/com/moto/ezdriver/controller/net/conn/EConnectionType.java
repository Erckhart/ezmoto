package com.moto.ezdriver.controller.net.conn;

public enum EConnectionType {

    DRIVER_UPDATE_POSITION("driver_update_position"),
    DRIVER_LOGIN("login.json"),
    DRIVER_SIGN_UP("users.json");

    private final String mUrl;

    public static final String BASE_URL = "http://ezmoto.cloudapp.net/mobile/";

    EConnectionType(final String url) {
        mUrl = url;
    }

    public String getUrl() {
        return BASE_URL + mUrl;
    }
}
