package com.moto.ezdriver.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.common.android.view.CommonFragment;
import com.moto.ezdriver.R;
import com.moto.ezdriver.controller.ControllerFragSignUp;

public class FragSignUp extends CommonFragment {

    private ControllerFragSignUp mController;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        return inflater.inflate(R.layout.frag_sign_up, container, false);
    }

    @Override
    public void onActivityCreated(final Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(savedInstanceState == null) {
            initializeActions();
        }
    }

    @Override
    public boolean onBackPressed() {
        mController.onBackPressed();
        return super.onBackPressed();
    }

    @Override
    public void initializeActions() {
        mController = new ControllerFragSignUp(this);
    }
}
