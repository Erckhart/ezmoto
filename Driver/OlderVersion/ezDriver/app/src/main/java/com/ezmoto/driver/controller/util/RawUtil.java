package com.ezmoto.driver.controller.util;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

public class RawUtil {

    public static void dial(final Context context, final String number) {
        if(!TextUtils.isEmpty(number)) {
            final Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("tel:" + number));
            context.startActivity(intent);
        }
    }

    public static String toSHA1(final String text) {
        return new String(Hex.encodeHex(DigestUtils.sha1(text)));
    }

}
