package com.ezmoto.driver.model.bo;

import com.ezmoto.driver.controller.util.RawUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.Serializable;

public class DriverBO implements Serializable {

    private int id;
    private String name;
    private LatLng position;
    private int ranking;
    private String photo;
    private String plate;
    private String[] phones;

    public DriverBO() {
        name = "Mauricio Souza";
        plate = "XXX-0000";
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public LatLng getPosition() {
        return position;
    }

    public int getRanking() {
        return ranking;
    }

    public String getPhoto() {
        return photo;
    }

    public String getPlate() {
        return plate;
    }

    @Override
    public String toString() {
        return name;
    }

    public MarkerOptions toMarkerOptions() {
        final MarkerOptions option = new MarkerOptions();
        option.position(position);
        option.title(name);
        //option.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_parent_driver_pin));
        return option;
    }

    public String[] getPhones() {
        return phones;
    }

    public String getCommunicationChannel() {
        final StringBuilder builder = new StringBuilder();
        builder.append(plate);
        builder.append(";");
        builder.append(name);
        builder.append(";");
        return RawUtil.toSHA1(builder.toString());
    }

}
