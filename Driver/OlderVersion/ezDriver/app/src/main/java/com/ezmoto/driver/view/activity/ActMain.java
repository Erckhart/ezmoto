package com.ezmoto.driver.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.ezmoto.driver.R;
import com.ezmoto.driver.controller.maps.MyLocationUtil;
import com.ezmoto.driver.controller.net.push.Push;
import com.ezmoto.driver.model.bo.DriverBO;
import com.google.android.gms.maps.model.LatLng;

public class ActMain extends AppCompatActivity implements Push.OnMessageReceiveListener, View.OnClickListener, MyLocationUtil.MyLocationListener {

    private Push mConnection;
    private int counter;
    private MyLocationUtil mLocationUtil;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);

        final DriverBO driver = new DriverBO();

        mLocationUtil = new MyLocationUtil(this);
        mLocationUtil.setMyLocationListener(this);
        mLocationUtil.setUpdatePosition(true);

        mConnection = new Push(driver.getCommunicationChannel());
        mConnection.setOnMessageReceiveListener(this);
        //mConnection.subscribe();

        findViewById(R.id.act_main_send_data).setOnClickListener(this);
    }

    @Override
    public void onMessageFailed() {
        Toast.makeText(this, "Falhou!!!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMessageReceived() {
        counter++;
        Toast.makeText(this, "Chupa que é de uva!!!" + counter, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(final View v) {
        mConnection.sendMessage("VAI PELO AMORDI DEUS!!!!!!!");
    }

    @Override
    public void onMyLocationFailed() {

    }

    @Override
    public void onMyLocationRetrieved(final LatLng position) {
        final String m_position = mountData(position);
        mConnection.sendMessage(m_position);
        Toast.makeText(this, "Position: " + m_position, Toast.LENGTH_SHORT).show();
    }

    private String mountData(final LatLng position) {
        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append("\"latitude\":");
        builder.append(position.latitude);
        builder.append(",");
        builder.append("\"longitude\":");
        builder.append(position.longitude);
        builder.append("}");
        return builder.toString();
    }

}
