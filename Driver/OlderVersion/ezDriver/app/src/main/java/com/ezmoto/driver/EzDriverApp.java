package com.ezmoto.driver;

import android.app.Application;

import com.ezmoto.driver.controller.net.push.Push;

public class EzDriverApp extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Push.initialize(this);
    }
}
