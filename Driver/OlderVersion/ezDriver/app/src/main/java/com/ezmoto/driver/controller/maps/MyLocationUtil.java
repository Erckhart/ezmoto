package com.ezmoto.driver.controller.maps;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

public class MyLocationUtil implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private final Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private MyLocationListener myLocationListener;

    private boolean mUpdatePosition;

    public MyLocationUtil(final Context context) {
        mContext = context;
        buildGoogleApiClient();
    }

    public void setMyLocationListener(final MyLocationListener myLocationListener) {
        this.myLocationListener = myLocationListener;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void moveTo(final GoogleMap map, final LatLng position) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
    }

    @Override
    public void onConnected(final Bundle bundle) {
        final Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (myLocationListener != null) {
            if (mLastLocation == null) {
                myLocationListener.onMyLocationFailed();
            } else {
                final LatLng position = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                myLocationListener.onMyLocationRetrieved(position);
            }
        }
        setUpdatePositionEnabled(mUpdatePosition);
    }


    @Override
    public void onConnectionSuspended(final int reason) {
        if (myLocationListener != null) {
            myLocationListener.onMyLocationFailed();
        }
    }

    @Override
    public void onConnectionFailed(final ConnectionResult connectionResult) {
        if (myLocationListener != null) {
            myLocationListener.onMyLocationFailed();
        }
    }

    public void setUpdatePosition(final boolean updatePosition) {
        mUpdatePosition = updatePosition;
        setUpdatePositionEnabled(mUpdatePosition);
    }

    private void setUpdatePositionEnabled(final boolean enabled) {
        if (mGoogleApiClient.isConnected()) {
            if (enabled) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, getLocationRequest(), this);
            } else {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            }
        }
    }

    private LocationRequest getLocationRequest() {
        final LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    @Override
    public void onLocationChanged(final Location location) {
        if (myLocationListener != null) {
            myLocationListener.onMyLocationRetrieved(new LatLng(location.getLatitude(), location.getLongitude()));
        }
    }

    public interface MyLocationListener {
        void onMyLocationFailed();

        void onMyLocationRetrieved(final LatLng position);
    }

}
