package com.ezmoto.driver.controller.receiver;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import com.parse.ParsePushBroadcastReceiver;

public class DriverPushReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected void onPushReceive(final Context context, final Intent intent) {
        //super.onPushReceive(context, intent);
        Toast.makeText(context,"PUSH RECEIVED",Toast.LENGTH_SHORT).show();
    }
}
