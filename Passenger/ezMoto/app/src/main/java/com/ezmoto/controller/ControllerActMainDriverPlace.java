package com.ezmoto.controller;

import android.content.Context;

import com.ezmoto.controller.maps.DirectionUtil;
import com.ezmoto.view.IView;
import com.ezmoto.view.dialog.DialogDriverArrived;
import com.google.android.gms.maps.model.LatLng;

public class ControllerActMainDriverPlace {

    private static final int DISTANCE_OFFSET = 150;

    private final IView mIView;

    private LatLng mRiderPosition;
    private LatLng mDriverPosition;
    private DirectionUtil mDirectionUtil;

    private OnDriverArrivedListener mOnDriverArrivedListener;

    private DialogDriverArrived mDialogDriverArrived;
    private boolean mCanceled;

    public ControllerActMainDriverPlace(final IView view) {
        mIView = view;
        mDirectionUtil = new DirectionUtil();
    }

    public void onRiderPositionUpdated(final LatLng position) {
        mRiderPosition = position;
        checkPosition();
    }

    public void onDriverPositionUpdated(final LatLng position) {
        mDriverPosition = position;
        checkPosition();
    }

    private void checkPosition() {
        if (mRiderPosition == null || mDriverPosition == null || mCanceled) {
            return;
        } else if (mDirectionUtil.distanceBetween(mDriverPosition, mRiderPosition) < DISTANCE_OFFSET && (mDialogDriverArrived == null || !mDialogDriverArrived.isShowing())) {
            mDialogDriverArrived = new DialogDriverArrived((Context) mIView);
            mDialogDriverArrived.show();
            if (mOnDriverArrivedListener != null) {
                mOnDriverArrivedListener.onDriverArrived();
            }
        }
    }

    public void setCanceled(final boolean canceled) {
        mCanceled = canceled;
    }

    public void setOnDriverArrivedListener(final OnDriverArrivedListener onDriverArrivedListener) {
        mOnDriverArrivedListener = onDriverArrivedListener;
    }

    public interface OnDriverArrivedListener {
        void onDriverArrived();
    }

}
