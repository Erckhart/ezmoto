package com.ezmoto.model.bo;

import com.ezmoto.model.enums.EPaymentOptions;
import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class TripBO implements Serializable {

    private long id;
    private LatLng origin;
    private LatLng destination;
    private EPaymentOptions payment;

    private String addressOrigin;
    private String addressDestination;

    public LatLng getOrigin() {
        return origin;
    }

    public void setOrigin(final LatLng origin) {
        this.origin = origin;
    }

    public LatLng getDestination() {
        return destination;
    }

    public void setDestination(final LatLng destination) {
        this.destination = destination;
    }

    public EPaymentOptions getPayment() {
        return payment;
    }

    public void setPayment(final EPaymentOptions payment) {
        this.payment = payment;
    }

    public String getAddressOrigin() {
        return addressOrigin;
    }

    public void setAddressOrigin(final String addressOrigin) {
        this.addressOrigin = addressOrigin;
    }

    public String getAddressDestination() {
        return addressDestination;
    }

    public void setAddressDestination(final String addressDestination) {
        this.addressDestination = addressDestination;
    }
}
