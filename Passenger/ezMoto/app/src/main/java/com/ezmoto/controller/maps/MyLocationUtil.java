package com.ezmoto.controller.maps;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;

import com.ezmoto.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MyLocationUtil implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private final Context mContext;
    private GoogleApiClient mGoogleApiClient;
    private MyLocationListener myLocationListener;

    public MyLocationUtil(final Context context) {
        mContext = context;
        buildGoogleApiClient();
    }

    public void setMyLocationListener(final MyLocationListener myLocationListener) {
        this.myLocationListener = myLocationListener;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public void moveTo(final GoogleMap map, final LatLng position) {
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(position, 15));
    }

    @Override
    public void onConnected(final Bundle bundle) {
        final Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (myLocationListener != null) {
            if (mLastLocation == null) {
                myLocationListener.onMyLocationFailed();
            } else {
                final LatLng position = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());
                final MarkerOptions options = getMyMarkerOptions(position);
                myLocationListener.onMyLocationRetrieved(options);
            }
        }
    }

    public static MarkerOptions getMyMarkerOptions(final LatLng position) {
        final MarkerOptions options = new MarkerOptions();
        options.position(position);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_parent_rider_pin));
        options.draggable(true);
        return options;
    }

    @Override
    public void onConnectionSuspended(final int reason) {
        if (myLocationListener != null) {
            myLocationListener.onMyLocationFailed();
        }
    }

    @Override
    public void onConnectionFailed(final ConnectionResult connectionResult) {
        if (myLocationListener != null) {
            myLocationListener.onMyLocationFailed();
        }
    }

    public interface MyLocationListener {
        void onMyLocationFailed();

        void onMyLocationRetrieved(final MarkerOptions options);
    }

}
