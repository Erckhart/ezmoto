package com.ezmoto.view.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RelativeLayout;

import com.ezmoto.R;

public class SlidingLayout extends RelativeLayout implements Animation.AnimationListener {

    public static final int NONE = -1;
    public static final int UP_TO_DOWN = 0;
    public static final int DOWN_TO_UP = 1;

    private Animation in;
    private Animation out;

    private int mDirection = NONE;

    public SlidingLayout(final Context context) {
        this(context, null);
    }

    public SlidingLayout(final Context context, final AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SlidingLayout(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SlidingLayout);
        mDirection = typedArray.getInteger(R.styleable.SlidingLayout_direction, mDirection);
        init();
    }

    private void init() {
        if (mDirection != NONE) {
            in = AnimationUtils.loadAnimation(getContext(), getInAnimation());
            in.setAnimationListener(this);
            out = AnimationUtils.loadAnimation(getContext(), getOutAnimation());
            out.setAnimationListener(this);
        }
    }

    private int getInAnimation() {
        switch (mDirection) {
            case UP_TO_DOWN:
                return R.anim.abc_slide_in_top;
            case DOWN_TO_UP:
                return R.anim.abc_slide_in_bottom;
        }
        return NONE;
    }

    private int getOutAnimation() {
        switch (mDirection) {
            case UP_TO_DOWN:
                return R.anim.abc_slide_out_top;
            case DOWN_TO_UP:
                return R.anim.abc_slide_out_bottom;
        }
        return NONE;
    }

    @Override
    public void setVisibility(final int visibility) {
        if (visibility == VISIBLE) {
            startAnimation(in);
        } else if (visibility == GONE) {
            startAnimation(out);
        }
    }

    @Override
    public void onAnimationStart(final Animation animation) {
        if (animation == in) {
            super.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onAnimationEnd(final Animation animation) {
        if (animation == out) {
            super.setVisibility(GONE);
        }
    }

    @Override
    public void onAnimationRepeat(final Animation animation) {

    }

    public void setDirection(final int direction) {
        mDirection = direction;
        init();
    }
}
