package com.ezmoto.model.enums;

import com.ezmoto.R;

public enum EPaymentOptions {

    MONEY(R.string.payment_option_money,-1),
    DEBIT(R.string.payment_option_debit,-1),
    CREDIT(R.string.payment_option_credit,-1);

    private final int mTitle;
    private final int mIcon;

    EPaymentOptions(final int title, final int icon) {
        mTitle = title;
        mIcon = icon;
    }

    public int getTitle() {
        return mTitle;
    }

    public int getIcon() {
        return mIcon;
    }
}
