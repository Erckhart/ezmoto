package com.ezmoto.controller.net.conn;

import com.ezmoto.controller.net.BaseConn;
import com.ezmoto.controller.net.exception.NetworkStatusException;
import com.ezmoto.model.bo.DriverBO;
import com.ezmoto.model.enums.ECommand;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;

import java.net.MalformedURLException;
import java.util.List;

public class ConnSearchDriver extends BaseConn {

    private final LatLng mLocation;

    public ConnSearchDriver(final LatLng location) {
        mLocation = location;
    }

    @Override
    public ECommand getCommand() throws MalformedURLException {
        return ECommand.SEARCH_DRIVERS;
    }

    @Override
    public void execute() throws NetworkStatusException {
        connect(METHOD_POST, MEDIUM_TIMEOUT, mountData());
    }

    @Override
    public List<DriverBO> getResult() throws NetworkStatusException, JsonSyntaxException {
        final String json = getData();
        final List<DriverBO> drivers = new Gson().fromJson(json, new TypeToken<List<DriverBO>>() {
        }.getType());
        return drivers;
    }

    private String mountData() {
        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append("\"latitude\":");
        builder.append(mLocation.latitude);
        builder.append(",");
        builder.append("\"longitude\":");
        builder.append(mLocation.longitude);
        builder.append("}");
        return builder.toString();
    }

}
