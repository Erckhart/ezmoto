/*
 * Copyright (C) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Mobile Communication Division,
 * Digital Media & Communications Business, Samsung Electronics Co., Ltd.
 *
 * This software and its documentation are confidential and proprietary
 * information of Samsung Electronics Co., Ltd.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to
 * any electronic medium or machine-readable form without the prior written
 * consent of Samsung Electronics.
 *
 * Samsung Electronics makes no representations with respect to the contents,
 * and assumes no responsibility for any errors that might appear in the
 * software and documents. This publication and the contents hereof are subject
 * to change without notice.
 */
package com.ezmoto.controller.task;

import android.content.Context;

import com.ezmoto.controller.maps.DirectionUtil;
import com.ezmoto.model.bo.EstimationBO;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TaskGetMapDirection extends AbsAsyncTask<LatLng, Void, List<List<HashMap<String, String>>>> {

    private final IMapDirection mMapDirection;
    private final DirectionUtil mDirectionUtil;
    private EstimationBO mEstimation;

    public TaskGetMapDirection(final Context context, final IMapDirection iMapDirection, final DirectionUtil directionUtil) {
        super(context);
        mMapDirection = iMapDirection;
        mDirectionUtil = directionUtil;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mMapDirection.onStartedRetrieveMapDirections();
    }

    @Override
    protected List<List<HashMap<String, String>>> doInBackground(final LatLng... params) {
        final List<List<HashMap<String, String>>> routes = new ArrayList<List<HashMap<String, String>>>();
        try {
            final LatLng from = params[0];
            final LatLng to = params[1];
            final String mapsApiDirectionsUrl = mDirectionUtil.getMapsApiDirectionsUrl(getContext(),from,to);
            final String jsonDirections = mDirectionUtil.readUrl(mapsApiDirectionsUrl);
            final JSONObject jsonObject = new JSONObject(jsonDirections);
            routes.addAll(mDirectionUtil.parse(jsonObject));
            mEstimation = mDirectionUtil.getDirectionEstimation(jsonObject);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return routes;
    }

    @Override
    protected void onPostExecute(final List<List<HashMap<String, String>>> lists) {
        super.onPostExecute(lists);
        if (!isCancelled()) {
            mMapDirection.onRetrievedMapDirections(mEstimation, lists);
        }
    }

    public interface IMapDirection {
        void onStartedRetrieveMapDirections();

        void onRetrievedMapDirections(final EstimationBO estimation, final List<List<HashMap<String, String>>> routes);
    }

}
