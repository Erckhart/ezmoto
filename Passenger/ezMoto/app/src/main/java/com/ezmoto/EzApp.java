package com.ezmoto;

import android.app.Application;
import android.os.AsyncTask;

import com.ezmoto.controller.net.push.Push;

import java.util.ArrayList;
import java.util.List;

public class EzApp extends Application {

    private static volatile List<AsyncTask<?, ?, ?>> tasks = new ArrayList<AsyncTask<?, ?, ?>>();

    public static void addTask(final AsyncTask<?, ?, ?> task) {
        tasks.add(task);
    }

    public static void removeTask(final AsyncTask<?, ?, ?> task) {
        tasks.remove(task);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Push.initialize(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        for (AsyncTask task : tasks) {
            task.cancel(true);
        }
    }
}
