package com.ezmoto.controller;

import android.view.View;

import com.ezmoto.model.bo.EstimationBO;
import com.ezmoto.view.IView;
import com.ezmoto.view.holder.HolderActMain;
import com.ezmoto.view.holder.HolderActMainEstimation;

public class ControllerActMainMapEstimation implements View.OnClickListener {

    private final HolderActMain mMainHolder;
    private final HolderActMainEstimation mHolderEstimation;
    private final IView mView;

    private boolean isShowing = false;
    private OnMapEstimationListener mOnMapEstimationListener;

    public ControllerActMainMapEstimation(final IView view, final HolderActMain holder) {
        mMainHolder = holder;
        mView = view;
        mHolderEstimation = new HolderActMainEstimation(mMainHolder.getEstimation());
        initializeActions();
    }

    private void initializeActions() {
        mHolderEstimation.getCallDriver().setOnClickListener(this);
        mHolderEstimation.getCancelDriver().setOnClickListener(this);
        mHolderEstimation.getStartTrip().setOnClickListener(this);
    }

    public void setEstimation(final EstimationBO estimation) {
        mHolderEstimation.putValues(estimation);
        show();
    }

    public void setLoadingEnabled(final boolean enabled) {
        if(enabled) {
            mHolderEstimation.getProgressBar().setVisibility(View.VISIBLE);
            mHolderEstimation.getCallDriver().setVisibility(View.GONE);
        } else {
            mHolderEstimation.getProgressBar().setVisibility(View.GONE);
            mHolderEstimation.getCallDriver().setVisibility(View.VISIBLE);
        }
    }

    private void show() {
        if (isShowing) {
            hide();
        }
        isShowing = true;
        mMainHolder.getEstimation().setVisibility(View.VISIBLE);
    }

    private void hide() {
        isShowing = false;
        mMainHolder.getEstimation().setVisibility(View.GONE);
    }

    public void onDriverArrived() {
        mHolderEstimation.getCallDriver().setVisibility(View.GONE);
        mHolderEstimation.getCancelDriver().setVisibility(View.GONE);
        mHolderEstimation.getStartTrip().setVisibility(View.VISIBLE);
        mHolderEstimation.getProgressBar().setVisibility(View.GONE);
    }

    @Override
    public void onClick(final View v) {
        if (v.getId() == mHolderEstimation.getCallDriver().getId()) {
            setLoadingEnabled(true);
            if(mOnMapEstimationListener != null) {
                mOnMapEstimationListener.onCallDriverClick();
            }
        } else if (v.getId() == mHolderEstimation.getCancelDriver().getId()) {
            setLoadingEnabled(false);
            setCancelable(false);
            if(mOnMapEstimationListener != null) {
                mOnMapEstimationListener.onCancelDriverClick();
            }
        } else if(v.getId() == mHolderEstimation.getStartTrip().getId()) {

            if(mOnMapEstimationListener != null) {
                mOnMapEstimationListener.onStartTripClick();
            }
        }
    }

    public void setOnMapEstimationListener(final OnMapEstimationListener onMapEstimationListener) {
        mOnMapEstimationListener = onMapEstimationListener;
    }

    public void setCancelable(final boolean cancelable) {
        if(cancelable) {
            mHolderEstimation.getCallDriver().setVisibility(View.GONE);
            mHolderEstimation.getCancelDriver().setVisibility(View.VISIBLE);
        } else {
            mHolderEstimation.getCallDriver().setVisibility(View.VISIBLE);
            mHolderEstimation.getCancelDriver().setVisibility(View.GONE);
        }
        mHolderEstimation.getProgressBar().setVisibility(View.GONE);
    }

    public interface OnMapEstimationListener {
        void onCallDriverClick();
        void onCancelDriverClick();
        void onStartTripClick();
    }

}
