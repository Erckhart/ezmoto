package com.ezmoto.view.holder;

import android.view.View;
import android.widget.TextView;

import com.ezmoto.R;
import com.ezmoto.view.AbsHolderItem;

public class HolderPhoneOptionsItem extends AbsHolderItem<String> {

    private TextView number;

    public HolderPhoneOptionsItem(final View view) {
        super(view);
    }

    @Override
    public void putValues(final String value) {
        number.setText(value);
    }

    @Override
    protected void initializeFields() {
        number = (TextView) getField(R.id.dialog_list_phone_options_text);
    }
}
