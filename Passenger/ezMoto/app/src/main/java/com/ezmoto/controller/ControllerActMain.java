package com.ezmoto.controller;

import android.support.v4.view.GravityCompat;
import android.view.View;
import android.widget.Toast;

import com.ezmoto.R;
import com.ezmoto.model.bo.DriverBO;
import com.ezmoto.model.bo.EstimationBO;
import com.ezmoto.model.bo.RiderBO;
import com.ezmoto.model.bo.TripBO;
import com.ezmoto.model.enums.ETripStatus;
import com.ezmoto.view.IView;
import com.ezmoto.view.holder.HolderActMain;
import com.google.android.gms.maps.model.LatLng;

public class ControllerActMain implements View.OnClickListener, ControllerActMainMapDirection.OnDirectionRetrievedListener, ControllerActMainTrip.OnTripChangeListener, ControllerActMainMapEstimation.OnMapEstimationListener, ControllerActMainDriver.OnDriverAcceptedListener, ControllerActMainDriverPlace.OnDriverArrivedListener {

    private static final long TIME_INTERVAL = 2000;

    private final IView mIView;
    private final HolderActMain mHolder;

    private final ControllerActMainTrip mControllerTrip;
    private final ControllerActMainMapDirection mControllerDirection;
    private final ControllerActMainMapEstimation mControllerEstimation;
    private final ControllerActMainDriver mControllerDriver;
    private final ControllerActMainDriverPlace mControllerDriverPlace;

    private RiderBO mRider;

    private ETripStatus mTripStatus;
    private long mBackPressedTime;

    public ControllerActMain(final IView iView) {
        mIView = iView;
        mHolder = new HolderActMain(mIView);
        setTripStatus(ETripStatus.STAND_BY);
        mControllerTrip = new ControllerActMainTrip(mIView, mHolder);
        mControllerDirection = new ControllerActMainMapDirection(mIView, mHolder);
        mControllerEstimation = new ControllerActMainMapEstimation(mIView, mHolder);
        mControllerDriver = new ControllerActMainDriver(mIView, mHolder);
        mControllerDriverPlace = new ControllerActMainDriverPlace(mIView);
        initializeView();
        initializeAction();
    }

    private void initializeAction() {
        mControllerDirection.setOnDirectionRetrievedListener(this);
        mControllerTrip.setOnTripChangeListener(this);
        mControllerEstimation.setOnMapEstimationListener(this);
        mControllerDriver.setOnDriverAcceptedListener(this);
        mControllerDriverPlace.setOnDriverArrivedListener(this);
        mHolder.getButtonsMainMenu().setOnClickListener(this);
    }

    private void initializeView() {
        mHolder.getMap().getUiSettings().setCompassEnabled(false);
        mHolder.getMap().getUiSettings().setMyLocationButtonEnabled(true);
        mIView.setSupportActionBar(mHolder.getToolbar());
        mIView.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mIView.getSupportActionBar().setHomeButtonEnabled(false);
        mIView.getSupportActionBar().setShowHideAnimationEnabled(false);
    }

    public void setTripStatus(final ETripStatus status) {
        mTripStatus = status;
        mHolder.putValues(mTripStatus);
    }

    @Override
    public void onClick(final View view) {
        final int id = view.getId();
        if (id == R.id.act_main_ib_main_menu) {
            mHolder.getDrawerLayout().openDrawer(GravityCompat.END);
        }
    }

    @Override
    public void onDirectionRetrieved(final EstimationBO estimation) {
        if (estimation != null) {
            mControllerEstimation.setEstimation(estimation);
        }
    }

    @Override
    public void onTripChange(final TripBO trip) {
        setTripStatus(ETripStatus.CHOOSING_TRIP);
        mControllerDriver.searchDrivers(trip.getOrigin());
        if (trip.getDestination() != null) {
            mControllerDirection.drawDirection(trip.getOrigin(), trip.getDestination());
        }
        mControllerDriverPlace.onRiderPositionUpdated(trip.getOrigin());
    }

    public boolean onBackPressed() {
        boolean handled = false;
        if (mHolder.getDrawerLayout().isDrawerOpen(GravityCompat.END)) {
            mHolder.getDrawerLayout().closeDrawer(GravityCompat.END);
            handled = true;
        } else if(mTripStatus == ETripStatus.DRIVER_ARRIVED) {
            handled = true;
            mControllerEstimation.setLoadingEnabled(false);
            mControllerEstimation.setCancelable(false);
            onCancelDriverClick();
        } else if (mBackPressedTime + TIME_INTERVAL < System.currentTimeMillis()) {
            handled = true;
            mBackPressedTime = System.currentTimeMillis();
            Toast.makeText(mIView.getBaseContext(),"Pressione novamente para sair", Toast.LENGTH_SHORT).show();
        }
        return handled;
    }

    @Override
    public void onCallDriverClick() {
        setTripStatus(ETripStatus.CALLING_DRIVER);
        mControllerDriver.callDriver(mControllerTrip.getTrip().getOrigin());
    }

    @Override
    public void onCancelDriverClick() {
        setTripStatus(ETripStatus.CHOOSING_TRIP);
        mControllerDriver.cancelDriver();
    }

    @Override
    public void onStartTripClick() {
        setTripStatus(ETripStatus.DRIVER_FOUND);
    }

    @Override
    public void onCallFailed() {
        setTripStatus(ETripStatus.CHOOSING_TRIP);
        mControllerEstimation.setLoadingEnabled(false);
        mControllerEstimation.setCancelable(false);
    }

    @Override
    public void onDriverAccepted(final DriverBO driver) {
        setTripStatus(ETripStatus.DRIVER_FOUND);
        mControllerEstimation.setLoadingEnabled(false);
        mControllerEstimation.setCancelable(true);
    }

    @Override
    public void onDriverPositionChanged(final LatLng newPosition) {
        mControllerDriverPlace.onDriverPositionUpdated(newPosition);
    }

    @Override
    public void onDriverArrived() {
        setTripStatus(ETripStatus.DRIVER_ARRIVED);
        mControllerEstimation.onDriverArrived();
        mControllerDriver.stopDriverTracking();
        mControllerDriverPlace.setCanceled(true);
    }
}
