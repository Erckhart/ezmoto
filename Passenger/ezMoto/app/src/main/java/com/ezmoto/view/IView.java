/*
 * Copyright (C) 2015 Samsung Electronics Co., Ltd. All rights reserved.
 *
 * Mobile Communication Division,
 * Digital Media & Communications Business, Samsung Electronics Co., Ltd.
 *
 * This software and its documentation are confidential and proprietary
 * information of Samsung Electronics Co., Ltd.  No part of the software and
 * documents may be copied, reproduced, transmitted, translated, or reduced to
 * any electronic medium or machine-readable form without the prior written
 * consent of Samsung Electronics.
 *
 * Samsung Electronics makes no representations with respect to the contents,
 * and assumes no responsibility for any errors that might appear in the
 * software and documents. This publication and the contents hereof are subject
 * to change without notice.
 */
package com.ezmoto.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.View;

public interface IView {

    /**
     * @see Activity#getBaseContext()
     * @return {@link Context}
     */
    Context getBaseContext();

    /**
     * @see Activity#getApplicationContext()
     * @return {@link Context}
     */
    Context getApplicationContext();

    /**
     * @see Activity#getAssets()
     * @return {@link AssetManager}
     */
    AssetManager getAssets();

    /**
     * @see Activity#getString(int)
     * @param resId
     * @return {@link String}
     */
    String getString(final int resId);

    /**
     * @see Activity#getResources()
     * @return {@link Resources}
     */
    Resources getResources();

    /**
     * @see Activity#findViewById(int)
     * @param id
     * @return {@link View}
     */
    View findViewById(final int mId);

    FragmentManager getSupportFragmentManager();
    
    /**
     * @see Activity#getSharedPreferences(String, int)
     * @param name
     * @param mode
     * @return {@link SharedPreferences}
     */
    SharedPreferences getSharedPreferences(final String name, final int mode);

    void startActivity(final Intent intent);

    public void initializeActions();

    void finish();
    
    void onBackPressed();
    
    void setResult(int resultCode, Intent data);
    
    Intent getIntent();

    void setSupportActionBar(final Toolbar toolbar);

    ActionBar getSupportActionBar();

}