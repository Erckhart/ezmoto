package com.ezmoto.view;

import android.app.Activity;
import android.content.Context;
import android.view.View;

public abstract class AbsHolderAct {

    protected IView mView;

    public AbsHolderAct(final IView view) {
	this.mView = view;
	initializeFields();
    }

    /**
     * initialize fields and finds
     */
    protected abstract void initializeFields();

    /**
     * performing the search components from the interface implemented by any
     * activity
     * 
     * @param resId
     * @return {@link View}
     */
    protected View getField(final int resId) {
	return mView.findViewById(resId);
    }

    /**
     * @see Activity#getString(int)
     * @param resId
     * @return {@link String}
     */
    public String getString(final int resId) {
	return mView.getString(resId);
    }

    /**
     * @see Activity#getApplicationContext()
     * @return {@link Context}
     */
    public Context getApplicationContext() {
	return mView.getApplicationContext();
    }

    public Context getBaseContext() {
	return mView.getBaseContext();
    }

}
