package com.ezmoto.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ezmoto.R;
import com.ezmoto.model.enums.EPaymentOptions;
import com.ezmoto.view.AbsBaseAdapter;
import com.ezmoto.view.holder.HolderPaymentTypeItem;

import java.util.Arrays;

public class AdpPayments extends AbsBaseAdapter<EPaymentOptions>{

    public AdpPayments(final Context context) {
        super(context);
        setItems(Arrays.asList(EPaymentOptions.values()));
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final HolderPaymentTypeItem holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_payment_options_item, null);
            holder = new HolderPaymentTypeItem(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HolderPaymentTypeItem) convertView.getTag();
        }
        holder.putValues(getItem(position));
        return convertView;
    }
}
