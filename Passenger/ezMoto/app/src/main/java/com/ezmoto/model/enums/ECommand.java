package com.ezmoto.model.enums;

public enum ECommand {

    SEARCH_DRIVERS("getDriversAroundMe"),
    CALL_DRIVER("callDriver");

    private final String mUrl;

    public static final String BASE_URL = "http://demo0461324.mockable.io/";

    ECommand(final String url) {
        mUrl = url;
    }

    public String getUrl() {
        return BASE_URL + mUrl;
    }
}
