package com.ezmoto.model.enums;

import com.ezmoto.R;

public enum ETripStatus {

    STAND_BY(R.string.status_stand_by, 0),
    CHOOSING_TRIP(R.string.status_choosing_trip, 0),
    CALLING_DRIVER(R.string.status_calling_driver, 0),
    DRIVER_FOUND(R.string.status_driver_found, 0),
    TRIP_INITIATED(R.string.status_trip_initiated, 0),
    DRIVER_ARRIVED(R.string.I02, 0);

    private final int mTitleResId;
    private final int mIconResId;

    ETripStatus(final int titleResId, final int iconResId) {
        mTitleResId = titleResId;
        mIconResId = titleResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getIconResId() {
        return mIconResId;
    }
}
