package com.ezmoto.model.enums;

import android.content.Context;

import com.ezmoto.R;

public enum EEzStatus {

    E01(R.string.E01),
    E02(R.string.E02),
    I01(R.string.I01);

    private final int mMessage;

    EEzStatus(final int msg) {
        mMessage = msg;
    }

    public int getMessage() {
        return mMessage;
    }

    public String getMessage(final Context context) {
        return context.getString(mMessage);
    }

}
