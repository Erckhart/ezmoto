package com.ezmoto.view;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;

public interface IFragView {

    public void initiliazeActions();

    public FragmentActivity getActivity();

    public LoaderManager getLoaderManager();

    public void startActivity(final Intent intent);

}
