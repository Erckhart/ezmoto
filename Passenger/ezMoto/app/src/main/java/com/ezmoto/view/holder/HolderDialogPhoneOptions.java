package com.ezmoto.view.holder;

import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.ezmoto.R;
import com.ezmoto.view.AbsHolderFrag;

public class HolderDialogPhoneOptions extends AbsHolderFrag {

    private ListView phones;

    public HolderDialogPhoneOptions(final View view) {
        super(view);
    }

    @Override
    protected void initializeFields() {
        phones = (ListView) getField(R.id.dialog_list_phone_options);
    }

    public ListView getPhones() {
        return phones;
    }
}
