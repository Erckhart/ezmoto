package com.ezmoto.view.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.ezmoto.R;
import com.ezmoto.controller.ControllerActMain;
import com.ezmoto.view.IActMain;

public class ActMain extends AppCompatActivity implements IActMain {

    private ControllerActMain mController;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        initializeActions();
    }

    @Override
    public void initializeActions() {
        mController = new ControllerActMain(this);
    }

    @Override
    public void onBackPressed() {
        final boolean handled = mController.onBackPressed();
        if(!handled) {
            super.onBackPressed();
        }
    }
}
