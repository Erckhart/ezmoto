package com.ezmoto.view.holder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.ezmoto.R;
import com.ezmoto.controller.util.CircleTransformation;
import com.ezmoto.model.bo.DriverBO;
import com.ezmoto.view.AbsHolderItem;
import com.ezmoto.view.dialog.DialogPhoneOptions;
import com.squareup.picasso.Picasso;

public class HolderActMainDriver extends AbsHolderItem<DriverBO> {

    private ImageView photo;
    private TextView name;
    private RatingBar rating;
    private TextView plate;
    private ImageButton callPhoneDriver;

    public HolderActMainDriver(final View view) {
        super(view);
    }

    @Override
    public void putValues(final DriverBO value) {
        Picasso.with(getContext()).load(value.getPhoto()).transform(new CircleTransformation(600, 50)).into(photo);
        name.setText(value.getName());
        rating.setRating(value.getRanking());
        plate.setText(value.getPlate());
    }

    @Override
    protected void initializeFields() {
        photo = (ImageView) getField(R.id.act_main_driver_photo);
        name = (TextView) getField(R.id.act_main_driver_name);
        rating = (RatingBar) getField(R.id.act_main_driver_rating);
        plate = (TextView) getField(R.id.act_main_driver_plate);
        callPhoneDriver = (ImageButton) getField(R.id.act_main_driver_phone_call);
    }

    public ImageButton getCallPhoneDriverButton() {
        return callPhoneDriver;
    }

    public DialogPhoneOptions getDialogPhoneOptions(final DriverBO driver, final DialogPhoneOptions.OnPhoneChooseListener onPhoneChooseListener) {
        return new DialogPhoneOptions(getContext(), driver, onPhoneChooseListener);
    }

}
