package com.ezmoto.view;

import android.view.View;

public abstract class AbsHolderItem<T> extends AbsHolderFrag {

    protected AbsHolderItem(final View view) {
	super(view);
    }

    public abstract void putValues(final T value);

}
