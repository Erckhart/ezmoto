package com.ezmoto.controller.task;

import android.content.Context;

import com.ezmoto.R;
import com.ezmoto.controller.net.conn.ConnSearchDriver;
import com.ezmoto.controller.net.exception.NetworkStatusException;
import com.ezmoto.model.bo.DriverBO;
import com.ezmoto.model.enums.EEzStatus;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;
import java.util.List;

public class TaskSearchDrivers extends AbsAsyncTask<LatLng, Void, List<DriverBO>> {

    private final ISearchDrivers listener;

    private EEzStatus status;

    public TaskSearchDrivers(final Context context, final ISearchDrivers listener) {
        super(context);
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (listener != null) {
            listener.onSearchDriversStart();
        }
    }

    @Override
    protected List<DriverBO> doInBackground(final LatLng... positions) {
        if(positions == null || positions.length == 0) {
            return new ArrayList<DriverBO>();
        } else {
            List<DriverBO> drivers = new ArrayList<DriverBO>();
            try {
                final ConnSearchDriver connection = new ConnSearchDriver(positions[0]);
                connection.execute();
                drivers = connection.getResult();
                status = EEzStatus.I01;
            } catch(final NetworkStatusException nse) {
                nse.printStackTrace();
                status = EEzStatus.E01;
            } catch (final JsonSyntaxException jse) {
                jse.printStackTrace();
                status = EEzStatus.E02;
            }
            return drivers;
        }
    }

    @Override
    protected void onPostExecute(final List<DriverBO> drivers) {
        super.onPostExecute(drivers);
        if (!isCancelled()) {
            if (listener != null) {
                if(status == EEzStatus.I01) {
                    finishTask(drivers);
                } else {
                    listener.onSearchDriversFailed(status);
                }
            }
        }
    }

    private void finishTask(final List<DriverBO> drivers) {
        final List<MarkerOptions> options = new ArrayList<MarkerOptions>();
        for (int i = 0; i < drivers.size(); i++) {
            final DriverBO driver = drivers.get(i);
            final MarkerOptions opt = new MarkerOptions();
            opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_parent_driver_pin));
            opt.position(driver.getPosition());
            opt.title(driver.getName());
            options.add(opt);
        }
        listener.onSearchDriversFinished(options);
    }

    public interface ISearchDrivers {

        void onSearchDriversStart();

        void onSearchDriversFailed(final EEzStatus status);

        void onSearchDriversFinished(final List<MarkerOptions> drivers);

    }


}
