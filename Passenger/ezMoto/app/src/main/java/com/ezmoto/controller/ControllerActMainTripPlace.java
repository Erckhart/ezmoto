package com.ezmoto.controller;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import com.ezmoto.view.IView;
import com.ezmoto.view.adapter.AdpAutoCompleteAddress;
import com.ezmoto.view.holder.HolderActMainTrip;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

public class ControllerActMainTripPlace implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, AdapterView.OnItemClickListener, ResultCallback<PlaceBuffer> {

    private final GoogleMap mMap;
    private final HolderActMainTrip mHolderActMainTrip;
    private final IView mView;

    private int selectedViewId = -1;
    private OnTripPlaceListener mOnTripPlaceListener;
    private GoogleApiClient mGoogleApiClient;
    private AdpAutoCompleteAddress mAdapter;

    public ControllerActMainTripPlace(final IView view, final GoogleMap map, final HolderActMainTrip holderActMainTrip) {
        mView = view;
        mMap = map;
        mHolderActMainTrip = holderActMainTrip;
        initializeActions();
    }

    private void initializeActions() {
        mHolderActMainTrip.getOrigin().setOnItemClickListener(this);
        mHolderActMainTrip.getDestination().setOnItemClickListener(this);
        mGoogleApiClient = new GoogleApiClient.Builder(mView.getBaseContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Places.GEO_DATA_API)
                .build();
        mGoogleApiClient.connect();
    }


    @Override
    public void onConnected(final Bundle bundle) {
        final LatLngBounds bounds = mMap.getProjection().getVisibleRegion().latLngBounds;
        mAdapter = new AdpAutoCompleteAddress(mView.getBaseContext(), android.R.layout.simple_list_item_1, mGoogleApiClient, bounds, null);
        mHolderActMainTrip.getOrigin().setAdapter(mAdapter);
        mHolderActMainTrip.getDestination().setAdapter(mAdapter);
    }

    @Override
    public void onConnectionSuspended(final int result) {
        Toast.makeText(mView.getBaseContext(),"onConnectionSuspended",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionFailed(final ConnectionResult connectionResult) {
        Toast.makeText(mView.getBaseContext(),"onConnectionFailed: "+connectionResult.toString(),Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
        selectedViewId = parent.getId();
        final AdpAutoCompleteAddress.PlaceAutocomplete item = mAdapter.getItem(position);
        PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, item.placeId.toString());
        placeResult.setResultCallback(this);
    }

    @Override
    public void onResult(final PlaceBuffer places) {
        if (!places.getStatus().isSuccess()) {
            // error
            return;
        } else if (places.getCount() > 0 && mOnTripPlaceListener != null) {
            final Place place = places.get(0);
            final LatLng position = place.getLatLng();
            mOnTripPlaceListener.onPlaceFound(position);
            selectedViewId = -1;
        } else {
            //error address not found
        }
    }

    public void setOnTripPlaceListener(OnTripPlaceListener onTripPlaceListener) {
        mOnTripPlaceListener = onTripPlaceListener;
    }

    public interface OnTripPlaceListener {
        void onPlaceFound(final LatLng position);
    }

}
