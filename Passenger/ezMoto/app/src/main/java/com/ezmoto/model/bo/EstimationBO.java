package com.ezmoto.model.bo;

import com.ezmoto.controller.util.SettingsUtil;

public class EstimationBO {

    private double distance;
    private double duration;

    public double getDistance() {
        return distance;
    }

    public double getDuration() {
        return duration;
    }

    public void setDistance(final double distance) {
        this.distance = distance;
    }

    public void setDuration(final double duration) {
        this.duration = duration;
    }

    public double getBalance() {
        final double taximeter = Double.valueOf(SettingsUtil.getInstance().getValue(SettingsUtil.TAXIMETER));
        final double on_sit = Double.valueOf(SettingsUtil.getInstance().getValue(SettingsUtil.TAXIMETER_SIT));
        return on_sit + (distance * taximeter);
    }

}
