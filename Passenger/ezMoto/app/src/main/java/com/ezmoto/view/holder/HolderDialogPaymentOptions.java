package com.ezmoto.view.holder;

import android.view.View;
import android.widget.ListView;

import com.ezmoto.R;
import com.ezmoto.view.AbsHolderFrag;

public class HolderDialogPaymentOptions extends AbsHolderFrag {

    private ListView paymentOptions;

    public HolderDialogPaymentOptions(final View view) {
        super(view);
    }

    @Override
    protected void initializeFields() {
        paymentOptions = (ListView) getField(R.id.dialog_list_payment_options);
    }

    public ListView getPaymentOptions() {
        return paymentOptions;
    }
}
