package com.ezmoto.controller.task;

import android.content.Context;

import com.ezmoto.controller.net.conn.ConnCallDriver;
import com.ezmoto.controller.net.exception.NetworkStatusException;
import com.ezmoto.model.bo.DriverBO;
import com.google.android.gms.maps.model.LatLng;

public class TaskCallDriver extends AbsAsyncTask<LatLng, Void, DriverBO> {

    private final OnCallDriverListener mOnCallDriverListener;

    public TaskCallDriver(final Context context, final OnCallDriverListener onCallDriverListener) {
        super(context);
        mOnCallDriverListener = onCallDriverListener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(mOnCallDriverListener != null) {
            mOnCallDriverListener.onCallStart();
        }
    }

    @Override
    protected DriverBO doInBackground(final LatLng... params) {
        DriverBO driver = null;
        try {
            ConnCallDriver connection = new ConnCallDriver(params[0]);
            connection.execute();
            driver = connection.getResult();
        } catch (NetworkStatusException e) {
            e.printStackTrace();
        }
        return driver;
    }

    @Override
    protected void onPostExecute(final DriverBO driverBO) {
        super.onPostExecute(driverBO);
        if(!isCancelled() && mOnCallDriverListener != null) {
            if(driverBO == null) {
                mOnCallDriverListener.onCallFailed();
            } else {
                mOnCallDriverListener.onDriverAccepted(driverBO);
            }
        }
    }

    public interface OnCallDriverListener {

        void onCallStart();

        void onCallFailed();

        void onDriverAccepted(final DriverBO driver);

    }

}
