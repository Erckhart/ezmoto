package com.ezmoto.controller;

import android.graphics.Color;

import com.ezmoto.controller.maps.DirectionUtil;
import com.ezmoto.controller.task.TaskGetMapDirection;
import com.ezmoto.model.bo.EstimationBO;
import com.ezmoto.view.IView;
import com.ezmoto.view.holder.HolderActMain;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ControllerActMainMapDirection implements TaskGetMapDirection.IMapDirection {

    private final IView mView;
    private final HolderActMain mHolder;
    private final DirectionUtil mDirectionUtil;
    private TaskGetMapDirection mTask;

    private Polyline route;
    private OnDirectionRetrievedListener mIActMainMapDirectionListener;

    public ControllerActMainMapDirection(final IView view, final HolderActMain holder) {
        mHolder = holder;
        mView = view;
        mDirectionUtil = new DirectionUtil();
    }

    public void drawDirection(final LatLng from, final LatLng to) {
        cancel();
        mTask = new TaskGetMapDirection(mView.getBaseContext(), this, mDirectionUtil);
        mTask.execute(from, to);
    }

    private void cancel() {
        if (mTask != null) {
            mTask.cancel(true);
        }
    }

    public void setOnDirectionRetrievedListener(final OnDirectionRetrievedListener mIActMainMapDirection) {
        this.mIActMainMapDirectionListener = mIActMainMapDirection;
    }

    @Override
    public void onStartedRetrieveMapDirections() {

    }

    @Override
    public void onRetrievedMapDirections(final EstimationBO estimation, final List<List<HashMap<String, String>>> routes) {
        final ArrayList<LatLng> points = new ArrayList<LatLng>();
        final PolylineOptions polyLineOptions = new PolylineOptions();

        // traversing through routes
        for (int i = 0; i < routes.size(); i++) {
            List<HashMap<String, String>> path = routes.get(i);

            for (int j = 0; j < path.size(); j++) {
                HashMap<String, String> point = path.get(j);

                double lat = Double.parseDouble(point.get("lat"));
                double lng = Double.parseDouble(point.get("lng"));
                LatLng position = new LatLng(lat, lng);

                points.add(position);
            }

            polyLineOptions.addAll(points);

        }
        if(!points.isEmpty()) {
            if(route == null) {
                polyLineOptions.width(5);
                polyLineOptions.color(Color.BLUE);
                route = mHolder.getMap().addPolyline(polyLineOptions);
            } else {
                route.getPoints().clear();
                route.setPoints(polyLineOptions.getPoints());
            }
        }
        if(mIActMainMapDirectionListener != null) {
            mIActMainMapDirectionListener.onDirectionRetrieved(estimation);
        }
    }

    public interface OnDirectionRetrievedListener {
        void onDirectionRetrieved(final EstimationBO estimation);
    }

}
