package com.ezmoto.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ezmoto.R;
import com.ezmoto.view.AbsBaseAdapter;
import com.ezmoto.view.holder.HolderPhoneOptionsItem;

import java.util.List;

public class AdpPhones extends AbsBaseAdapter<String> {

    public AdpPhones(final List<String> source, final Context context) {
        super(source, context);
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final HolderPhoneOptionsItem holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dialog_phone_options_item, null);
            holder = new HolderPhoneOptionsItem(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HolderPhoneOptionsItem) convertView.getTag();
        }
        holder.putValues(getItem(position));
        return convertView;
    }
}
