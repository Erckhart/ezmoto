package com.ezmoto.controller.util;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;

import com.squareup.picasso.Transformation;

public class CircleTransformation implements Transformation {

    private static final String KEY_TRANSFORMATION = "KEY_CIRCLE";
    // dimensions in DP
    private final int mRadius;
    private final int mMargin;

    public CircleTransformation(final int radius, final int margin) {
        mRadius = radius;
        mMargin = margin;
    }

    @Override
    public String key() {
        return KEY_TRANSFORMATION;
    }

    @Override
    public Bitmap transform(final Bitmap bitmap) {
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setShader(new BitmapShader(bitmap, TileMode.CLAMP, TileMode.CLAMP));

        final Bitmap out = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(),
                Config.ARGB_8888);
        final Canvas canvas = new Canvas(out);
        canvas.drawRoundRect(new RectF(mMargin, mMargin, bitmap.getWidth()
                        - mMargin, bitmap.getHeight() - mMargin), mRadius, mRadius,
                paint);

        if (bitmap != out) {
            bitmap.recycle();
        }
        return out;
    }

}
