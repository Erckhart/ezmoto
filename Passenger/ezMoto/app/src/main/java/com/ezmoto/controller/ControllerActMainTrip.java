package com.ezmoto.controller;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ezmoto.R;
import com.ezmoto.controller.maps.DirectionUtil;
import com.ezmoto.controller.maps.MyLocationUtil;
import com.ezmoto.model.bo.TripBO;
import com.ezmoto.model.enums.EPaymentOptions;
import com.ezmoto.view.IView;
import com.ezmoto.view.dialog.DialogPaymentOptions;
import com.ezmoto.view.holder.HolderActMain;
import com.ezmoto.view.holder.HolderActMainTrip;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class ControllerActMainTrip implements GoogleMap.OnMarkerDragListener, GoogleMap.OnMapLongClickListener, MyLocationUtil.MyLocationListener, View.OnClickListener, ControllerActMainTripPlace.OnTripPlaceListener, ActionMode.Callback, DialogPaymentOptions.OnPaymentChooseListener {

    private final IView mIView;
    private final HolderActMainTrip mHolderTrip;
    private final HolderActMain mMainHolder;
    private final MyLocationUtil mGpsUtil;

    private OnTripChangeListener mOnTripChangeListener;

    private TripBO mTrip;

    private Marker myOriginMarker;
    private Marker myDestinationMarker;
    private DirectionUtil mDirectionUtil;

    private ControllerActMainTripPlace mControllerActMainTripPlace;

    public ControllerActMainTrip(final IView iView, final HolderActMain holder) {
        mIView = iView;
        mMainHolder = holder;
        mTrip = new TripBO();
        mDirectionUtil = new DirectionUtil();
        mHolderTrip = new HolderActMainTrip(holder.getRiderOptions());
        mControllerActMainTripPlace = new ControllerActMainTripPlace(mIView, mMainHolder.getMap(), mHolderTrip);
        mGpsUtil = new MyLocationUtil(mIView.getBaseContext());
        initializePaymentOptions();
        initializeAction();
    }

    private void initializeAction() {
        mControllerActMainTripPlace.setOnTripPlaceListener(this);
        mMainHolder.getMap().setOnMarkerDragListener(this);
        mMainHolder.getMap().setOnMapLongClickListener(this);
        mGpsUtil.setMyLocationListener(this);
        mHolderTrip.getPaymentOption().setOnClickListener(this);
    }


    private void initializePaymentOptions() {
        mHolderTrip.getPaymentOption().setCustomSelectionActionModeCallback(this);
    }

    public TripBO getTrip() {
        return mTrip;
    }

    public void setOnTripChangeListener(final OnTripChangeListener onTripChangeListener) {
        mOnTripChangeListener = onTripChangeListener;
    }

    private void setMyLocation(final LatLng myLocation) {
        if (myOriginMarker == null) {
            myOriginMarker = mMainHolder.getMap().addMarker(MyLocationUtil.getMyMarkerOptions(myLocation));
        } else {
            myOriginMarker.setPosition(myLocation);
        }
        mTrip.setOrigin(myLocation);
        final String address = mDirectionUtil.getAddress(mIView.getBaseContext(), myLocation);
        mTrip.setAddressOrigin(address);
        mHolderTrip.putValues(mTrip);
        if (mOnTripChangeListener != null) {
            mOnTripChangeListener.onTripChange(mTrip);
        }
    }

    private void setMyDestination(final LatLng myDestination) {
        if (myDestinationMarker == null) {
            final MarkerOptions opt = new MarkerOptions();
            opt.position(myDestination);
            opt.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_parent_rider_pin));
            myDestinationMarker = mMainHolder.getMap().addMarker(opt);
        } else {
            myDestinationMarker.setPosition(myDestination);
        }
        mTrip.setDestination(myDestination);
        final String address = mDirectionUtil.getAddress(mIView.getBaseContext(), myDestination);
        mTrip.setAddressDestination(address);
        mHolderTrip.putValues(mTrip);
        if (mOnTripChangeListener != null) {
            mOnTripChangeListener.onTripChange(mTrip);
        }
    }

    @Override
    public void onMarkerDragStart(final Marker marker) {

    }

    @Override
    public void onMarkerDrag(final Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(final Marker marker) {
        setMyLocation(marker.getPosition());
    }

    @Override
    public void onMapLongClick(final LatLng latLng) {
        setMyDestination(latLng);
    }

    @Override
    public void onMyLocationFailed() {

    }

    @Override
    public void onMyLocationRetrieved(final MarkerOptions options) {
        myOriginMarker = mMainHolder.getMap().addMarker(options);
        mGpsUtil.moveTo(mMainHolder.getMap(), options.getPosition());
        setMyLocation(options.getPosition());
    }

    @Override
    public void onClick(final View v) {
        mHolderTrip.getDialogPaymentOptions(this).show();
    }

    @Override
    public void onPlaceFound(final LatLng position) {
        if (mHolderTrip.getOrigin().isFocused()) {
            setMyLocation(position);
        } else if (mHolderTrip.getDestination().isFocused()) {
            setMyDestination(position);
        }
    }

    @Override
    public boolean onCreateActionMode(final ActionMode mode, final Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareActionMode(final ActionMode mode, final Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(final ActionMode mode, final MenuItem item) {
        return false;
    }

    @Override
    public void onDestroyActionMode(final ActionMode mode) {
        // do nothing
    }

    @Override
    public void onPaymentChoose(final EPaymentOptions option) {
        mTrip.setPayment(option);
        mHolderTrip.putValues(mTrip);
    }

    public interface OnTripChangeListener {
        void onTripChange(final TripBO trip);
    }
}
