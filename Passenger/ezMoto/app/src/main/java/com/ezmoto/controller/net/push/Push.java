package com.ezmoto.controller.net.push;

import android.content.Context;

import com.ezmoto.R;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;

public class Push {

    private static final int PUSH_EXPIRATION_INTERVAL = 300; //seconds

    private OnMessageReceiveListener mOnMessageReceiveListener;
    private String mSubscriber;

    private SaveCallback subscribeCallback = new SaveCallback() {
        @Override
        public void done(final ParseException e) {
            if (mOnMessageReceiveListener != null) {
                if (e == null) {
                    mOnMessageReceiveListener.onMessageReceived();
                } else {
                    mSubscriber = null;
                    mOnMessageReceiveListener.onMessageFailed();
                }
            }
        }
    };

    public static void initialize(final Context context) {
        final String app_id = context.getString(R.string.parse_app_key);
        final String client_id = context.getString(R.string.parse_client_key);
        Parse.initialize(context, app_id, client_id);
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }

    public void subscribe(final String channel) {
        ParsePush.subscribeInBackground(channel, subscribeCallback);
        mSubscriber = channel;
    }

    public void unsubscribe() {
        if (mSubscriber != null) {
            ParsePush.unsubscribeInBackground(mSubscriber);
        }
    }

    public void setOnMessageReceiveListener(final OnMessageReceiveListener onMessageReceiveListener) {
        mOnMessageReceiveListener = onMessageReceiveListener;
    }

    public void sendMessage(final String message) {
        ParsePush push = new ParsePush();
        push.setChannel(mSubscriber);
        push.setMessage(message);
        push.setExpirationTimeInterval(PUSH_EXPIRATION_INTERVAL);
        push.sendInBackground();
    }

    public interface OnMessageReceiveListener {
        void onMessageFailed();

        void onMessageReceived();
    }

}
