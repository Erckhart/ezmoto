package com.ezmoto.controller.receiver;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.ezmoto.controller.ControllerActMainDriver;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

public class DriverPositionReceiver extends ParsePushBroadcastReceiver {

    @Override
    protected void onPushReceive(final Context context, final Intent intent) {
        if (intent.hasExtra("com.parse.Data")) {
            try {
                final JSONObject json = new JSONObject(intent.getStringExtra("com.parse.Data"));
                final LatLng new_driver_position = new Gson().fromJson(json.getString("alert"), LatLng.class);

                final Intent driver_updated_position_intent = new Intent(ControllerActMainDriver.ACTION_DRIVER_POSITION);
                driver_updated_position_intent.putExtra(ControllerActMainDriver.ACTION_DRIVER_POSITION_KEY, new_driver_position);

                LocalBroadcastManager.getInstance(context).sendBroadcast(driver_updated_position_intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
