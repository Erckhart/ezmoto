package com.ezmoto.view.holder;

import android.view.View;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.ezmoto.R;
import com.ezmoto.model.enums.EPaymentOptions;
import com.ezmoto.view.AbsHolderItem;

public class HolderPaymentTypeItem extends AbsHolderItem<EPaymentOptions> {

    private TextView paymentOption;

    public HolderPaymentTypeItem(final View view) {
        super(view);
    }

    @Override
    public void putValues(final EPaymentOptions value) {
        paymentOption.setText(value.getTitle());
    }


    @Override
    protected void initializeFields() {
        paymentOption = (TextView) getField(R.id.dialog_list_payment_options_text);
    }
}
