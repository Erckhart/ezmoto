package com.ezmoto.view.widget;

import android.content.Context;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.ezmoto.R;

public class SlidingToolbar extends Toolbar implements Animation.AnimationListener {

    private Animation in;
    private Animation out;

    public SlidingToolbar(final Context context) {
        this(context, null);
    }

    public SlidingToolbar(final Context context, final AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.toolbarStyle);
    }

    public SlidingToolbar(final Context context, final AttributeSet attrs, final int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        in = AnimationUtils.loadAnimation(getContext(), R.anim.abc_slide_in_top);
        in.setAnimationListener(this);
        out = AnimationUtils.loadAnimation(getContext(), R.anim.abc_slide_out_top);
        out.setAnimationListener(this);
    }

    @Override
    public void setVisibility(final int visibility) {
        if (visibility == VISIBLE) {
            startAnimation(in);
        } else if (visibility == GONE) {
            startAnimation(out);
        }
    }

    @Override
    public void onAnimationStart(final Animation animation) {
        if (animation == in) {
            super.setVisibility(VISIBLE);
        }
    }

    @Override
    public void onAnimationEnd(final Animation animation) {
        if (animation == out) {
            super.setVisibility(GONE);
        }
    }

    @Override
    public void onAnimationRepeat(final Animation animation) {

    }

}
