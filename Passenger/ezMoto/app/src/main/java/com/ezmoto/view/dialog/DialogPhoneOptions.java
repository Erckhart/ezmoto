package com.ezmoto.view.dialog;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.ezmoto.R;
import com.ezmoto.model.bo.DriverBO;
import com.ezmoto.view.adapter.AdpPhones;
import com.ezmoto.view.holder.HolderDialogPhoneOptions;

import java.util.Arrays;

public class DialogPhoneOptions extends BaseDialog implements AdapterView.OnItemClickListener {

    private final DriverBO mDriver;
    private final OnPhoneChooseListener mOnPhoneChooseListener;

    private HolderDialogPhoneOptions mHolder;
    private AdpPhones mAdapter;

    public DialogPhoneOptions(final Context context, final DriverBO driver, final OnPhoneChooseListener onPhoneChooseListener) {
        super(context, R.layout.dialog_phone_options);
        mDriver = driver;
        mOnPhoneChooseListener = onPhoneChooseListener;
        initialize();
        initializeActions();
    }

    private void initializeActions() {
        mHolder.getPhones().setOnItemClickListener(this);
    }

    @Override
    protected void initialize() {
        mHolder = new HolderDialogPhoneOptions(mView);
        mAdapter = new AdpPhones(Arrays.asList(mDriver.getPhones()), getContext());
        mHolder.getPhones().setAdapter(mAdapter);
    }

    @Override
    public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
        if(mOnPhoneChooseListener != null) {
            mOnPhoneChooseListener.onPhoneChosen(mAdapter.getItem(position));
        }
        dismiss();
    }

    public interface OnPhoneChooseListener {
        void onPhoneChosen(final String number);
    }

}
