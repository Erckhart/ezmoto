package com.ezmoto.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.ezmoto.controller.maps.DirectionUtil;
import com.ezmoto.controller.net.push.Push;
import com.ezmoto.controller.task.TaskCallDriver;
import com.ezmoto.controller.task.TaskSearchDrivers;
import com.ezmoto.controller.util.RawUtil;
import com.ezmoto.model.bo.DriverBO;
import com.ezmoto.model.enums.EEzStatus;
import com.ezmoto.view.IView;
import com.ezmoto.view.dialog.DialogPhoneOptions;
import com.ezmoto.view.holder.HolderActMain;
import com.ezmoto.view.holder.HolderActMainDriver;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

public class ControllerActMainDriver extends BroadcastReceiver implements TaskSearchDrivers.ISearchDrivers, TaskCallDriver.OnCallDriverListener, View.OnClickListener, DialogPhoneOptions.OnPhoneChooseListener {

    public static final String ACTION_DRIVER_POSITION = "com.ezmoto.DRIVER_POSITION_UPDATE";
    public static final String ACTION_DRIVER_POSITION_KEY = "com.ezmoto.DRIVER_POSITION_UPDATE_KEY";

    private final IView mIView;
    private final HolderActMain mHolder;
    private final HolderActMainDriver mHolderActMainDriver;

    private final Push mPush;
    private final DirectionUtil mDirectionUtil;

    private TaskSearchDrivers mTaskSearchDrivers;
    private TaskCallDriver mTaskCallDriver;

    private OnDriverAcceptedListener mOnDriverAcceptedListener;
    private List<Marker> mDriversMarkers;
    private Marker mDriverMarker;
    private DriverBO mDriver;


    public ControllerActMainDriver(final IView iView, final HolderActMain holder) {
        mIView = iView;
        mHolder = holder;
        mHolderActMainDriver = new HolderActMainDriver(mHolder.getDriverProfile());
        mDriversMarkers = new ArrayList<Marker>();
        mPush = new Push();
        mDirectionUtil = new DirectionUtil();
        initializeAction();
    }

    private void initializeAction() {
        mHolderActMainDriver.getCallPhoneDriverButton().setOnClickListener(this);
    }

    public void searchDrivers(final LatLng location) {
        if (mTaskSearchDrivers != null) {
            mTaskSearchDrivers.cancel(true);
        }
        mTaskSearchDrivers = new TaskSearchDrivers(mIView.getBaseContext(), this);
        mTaskSearchDrivers.execute(location);
    }

    public void callDriver(final LatLng position) {
        if (mTaskCallDriver != null) {
            mTaskCallDriver.cancel(true);
        }
        mTaskCallDriver = new TaskCallDriver(mIView.getBaseContext(), this);
        mTaskCallDriver.execute(position);
    }

    @Override
    public void onSearchDriversStart() {

    }

    @Override
    public void onSearchDriversFailed(final EEzStatus error) {

    }

    @Override
    public void onSearchDriversFinished(final List<MarkerOptions> drivers) {
        for (int i = 0; i < drivers.size(); i++) {
            final MarkerOptions option = drivers.get(i);
            final Marker marker = mHolder.getMap().addMarker(option);
            mDriversMarkers.add(marker);
        }
    }

    @Override
    public void onCallStart() {

    }

    @Override
    public void onCallFailed() {
        if (mOnDriverAcceptedListener != null) {
            mOnDriverAcceptedListener.onCallFailed();
        }
    }

    @Override
    public void onDriverAccepted(final DriverBO driver) {
        for (int i = 0; i < mDriversMarkers.size(); i++) {
            mDriversMarkers.get(i).setVisible(false);
        }
        mDriver = driver;
        mDriverMarker = mHolder.getMap().addMarker(driver.toMarkerOptions());
        mHolderActMainDriver.putValues(mDriver);
        setCanceled(false);
        if (mOnDriverAcceptedListener != null) {
            mOnDriverAcceptedListener.onDriverAccepted(driver);
        }
        mPush.subscribe(mDriver.getCommunicationChannel());
    }

    public void cancelDriver() {
        mDriver = null;
        mDriverMarker.remove();
        mDriverMarker = null;
        for (int i = 0; i < mDriversMarkers.size(); i++) {
            mDriversMarkers.get(i).setVisible(true);
        }
        setCanceled(true);
    }

    public void stopDriverTracking() {
        if(mPush != null) {
            mPush.unsubscribe();
        }
    }

    private void setReceiverEnabled(final boolean enabled) {
        if (enabled) {
            LocalBroadcastManager.getInstance(mIView.getBaseContext()).registerReceiver(this, new IntentFilter(ACTION_DRIVER_POSITION));
        } else {
            LocalBroadcastManager.getInstance(mIView.getBaseContext()).unregisterReceiver(this);
        }
    }

    public void setCanceled(final boolean canceled) {
        if (canceled) {
            mHolder.getDriverProfile().setVisibility(View.GONE);
            mHolder.getToolbar().setVisibility(View.VISIBLE);
            mHolder.getRiderOptions().setVisibility(View.VISIBLE);
            mPush.unsubscribe();
        } else {
            mHolder.getDriverProfile().setVisibility(View.VISIBLE);
            mHolder.getToolbar().setVisibility(View.GONE);
            mHolder.getRiderOptions().setVisibility(View.GONE);
        }
        setReceiverEnabled(!canceled);
    }

    public void setOnDriverAcceptedListener(final OnDriverAcceptedListener onDriverAcceptedListener) {
        mOnDriverAcceptedListener = onDriverAcceptedListener;
    }

    @Override
    public void onClick(final View v) {
        if (mDriver != null) {
            if (mDriver.getPhones().length > 0) {
                if (mDriver.getPhones().length == 1) {
                    RawUtil.dial(mIView.getBaseContext(), mDriver.getPhones()[0]);
                } else {
                    mHolderActMainDriver.getDialogPhoneOptions(mDriver, this).show();
                }
            }
        }
    }

    @Override
    public void onPhoneChosen(final String number) {
        RawUtil.dial(mIView.getBaseContext(), number);
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        if (intent.hasExtra(ACTION_DRIVER_POSITION_KEY)) {
            final LatLng new_driver_position = intent.getParcelableExtra(ACTION_DRIVER_POSITION_KEY);
            mDirectionUtil.animateMarker(mDriverMarker, new_driver_position, mHolder.getMap().getProjection(), false);
            mOnDriverAcceptedListener.onDriverPositionChanged(new_driver_position);
        }
    }

    public interface OnDriverAcceptedListener {
        void onCallFailed();

        void onDriverAccepted(final DriverBO driver);

        void onDriverPositionChanged(final LatLng newPosition);
    }

}
