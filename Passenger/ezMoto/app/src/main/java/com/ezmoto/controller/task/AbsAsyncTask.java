package com.ezmoto.controller.task;

import android.content.Context;
import android.os.AsyncTask;

import com.ezmoto.EzApp;

abstract class AbsAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {

    private final Context context;

    public AbsAsyncTask(final Context context) {
        this.context = context;
    }

    public Context getContext() {
        return context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        EzApp.addTask(this);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        EzApp.removeTask(this);
    }

    @Override
    protected void onPostExecute(final Result result) {
        super.onPostExecute(result);
        EzApp.removeTask(this);
    }

}
