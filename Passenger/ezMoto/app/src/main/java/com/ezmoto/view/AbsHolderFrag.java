package com.ezmoto.view;

import android.content.Context;
import android.view.View;

public abstract class AbsHolderFrag {

    protected View mView;

    protected AbsHolderFrag(final View view) {
	super();
	this.mView = view;
	initializeFields();
    }

    /**
     * initialize fields and finds
     */
    protected abstract void initializeFields();

    /**
     * performing the search components from the interface implemented by any
     * activity
     * 
     * @param resId
     * @return {@link View}
     */
    protected View getField(final int resId) {
	return mView.findViewById(resId);
    }

    protected Context getContext() {
	return mView.getContext();
    }

}
