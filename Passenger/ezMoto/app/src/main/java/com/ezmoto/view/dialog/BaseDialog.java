package com.ezmoto.view.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;

import com.ezmoto.R;

public abstract class BaseDialog {
    protected AlertDialog.Builder builder;
    protected AlertDialog mDialog = null;
    protected View mView;
    protected Context mContext;

    private DialogInterface.OnDismissListener mOnDismissListener;

    public BaseDialog(final Context context, final int layout) {
        mContext = context;
        configure(layout);
        initialize();
    }

    private void configure(final int layout) {
        builder = new AlertDialog.Builder(mContext, R.style.Base_Theme_AppCompat_Dialog);
        builder.setCancelable(true);
        final LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = inflater.inflate(layout, null);
        builder.setView(mView);
    }

    public Context getContext() {
        return mContext;
    }

    public View getView() {
        return mView;
    }

    public void setOnDismissListener(final DialogInterface.OnDismissListener listener) {
        mOnDismissListener = listener;
    }

    public void show() {
        mDialog = builder.create();
        mDialog.setCanceledOnTouchOutside(true);
        mDialog.setOnDismissListener(mOnDismissListener);
        mDialog.show();
        mDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void dismiss() {
        if (isShowing()) {
            mDialog.dismiss();
        }
    }

    public boolean isShowing() {
        return mDialog != null && mDialog.isShowing();
    }

    protected abstract void initialize();

}
