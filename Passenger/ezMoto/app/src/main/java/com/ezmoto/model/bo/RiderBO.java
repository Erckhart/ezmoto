package com.ezmoto.model.bo;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class RiderBO implements Serializable {

    private int id;
    private String name;
    private String photo;
    private LatLng position;
    private TripBO trip;

    public void setPosition(final LatLng position) {
        this.position = position;
    }

    public LatLng getPosition() {
        return position;
    }

    public String getName() {
        return name;
    }

    public TripBO getTrip() {
        return trip;
    }

    public void setTrip(final TripBO trip) {
        this.trip = trip;
    }


}
