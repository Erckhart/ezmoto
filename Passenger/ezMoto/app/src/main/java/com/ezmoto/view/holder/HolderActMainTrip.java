package com.ezmoto.view.holder;

import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import com.ezmoto.R;
import com.ezmoto.model.bo.TripBO;
import com.ezmoto.view.AbsHolderItem;
import com.ezmoto.view.dialog.DialogPaymentOptions;

public class HolderActMainTrip extends AbsHolderItem<TripBO> {

    private AutoCompleteTextView origin;
    private AutoCompleteTextView destination;
    private EditText paymentOptions;

    public HolderActMainTrip(final View view) {
        super(view);
    }

    @Override
    public void putValues(final TripBO value) {

        if (!TextUtils.isEmpty(value.getAddressOrigin())) {
            origin.setText(value.getAddressOrigin());
        }

        if (!TextUtils.isEmpty(value.getAddressDestination())) {
            destination.setText(value.getAddressDestination());
        }

        if (value.getPayment() != null) {
            paymentOptions.setText(value.getPayment().getTitle());
        }
    }

    @Override
    protected void initializeFields() {
        origin = (AutoCompleteTextView) getField(R.id.act_main_origin);
        origin.setThreshold(6);
        destination = (AutoCompleteTextView) getField(R.id.act_main_destiny);
        destination.setThreshold(6);
        paymentOptions = (EditText) getField(R.id.act_main_rider_payments);
    }

    public DialogPaymentOptions getDialogPaymentOptions(final DialogPaymentOptions.OnPaymentChooseListener onPaymentChooseListener) {
        return new DialogPaymentOptions(getContext(), onPaymentChooseListener);
    }

    public EditText getPaymentOption() {
        return paymentOptions;
    }

    public AutoCompleteTextView getDestination() {
        return destination;
    }

    public AutoCompleteTextView getOrigin() {
        return origin;
    }
}
