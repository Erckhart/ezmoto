package com.ezmoto.view.dialog;

import android.content.Context;
import android.os.Vibrator;
import android.view.View;
import android.widget.Button;

import com.ezmoto.R;

public class DialogDriverArrived extends BaseDialog implements View.OnClickListener {

    private static final long[] VIBRATION_TIME = {100L, 400L, 100L, 400L};
    private static final int VIBRATION_REPEAT_COUNT = 0; //

    private Vibrator vibrator;

    public DialogDriverArrived(final Context context) {
        super(context, R.layout.dialog_driver_arrived);
    }

    @Override
    protected void initialize() {
        final Button okButton = (Button) getView().findViewById(R.id.dialog_driver_arrived_info_button);
        okButton.setOnClickListener(this);
        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(VIBRATION_TIME, VIBRATION_REPEAT_COUNT);
    }

    @Override
    public void onClick(final View v) {
        dismiss();
    }

    @Override
    public void dismiss() {
        vibrator.cancel();
        super.dismiss();
    }
}
