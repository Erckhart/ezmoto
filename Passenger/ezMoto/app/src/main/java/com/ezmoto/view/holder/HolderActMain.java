package com.ezmoto.view.holder;

import android.app.Activity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ezmoto.R;
import com.ezmoto.model.enums.ETripStatus;
import com.ezmoto.view.AbsHolderAct;
import com.ezmoto.view.IView;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;

public class HolderActMain extends AbsHolderAct {

    private GoogleMap map;
    private Toolbar toolbar;
    private ImageButton mainMenu;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggleListener;

    private TextView status;
    private ImageView statusIcon;

    private View estimation;
    private View riderOptions;
    private View driverProfile;

    public HolderActMain(final IView view) {
        super(view);
    }

    @Override
    protected void initializeFields() {
        map = ((SupportMapFragment) mView.getSupportFragmentManager().findFragmentById(R.id.act_main_map)).getMap();
        mainMenu = (ImageButton) getField(R.id.act_main_ib_main_menu);
        toolbar = (Toolbar) mView.findViewById(R.id.act_main_toolbar);
        status = (TextView) getField(R.id.act_main_status);
        statusIcon = (ImageView) getField(R.id.act_main_status_icon);
        estimation = getField(R.id.act_main_estimation);
        riderOptions = getField(R.id.act_main_rider_options);
        driverProfile = getField(R.id.act_main_driver_profile);
        drawerToggleListener = new ActionBarDrawerToggle((Activity) mView, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout = (DrawerLayout) mView.findViewById(R.id.drawer_layout);
        drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        drawerLayout.setDrawerListener(drawerToggleListener);
    }

    public GoogleMap getMap() {
        return map;
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    public ActionBarDrawerToggle getDrawerToggleListener() {
        return drawerToggleListener;
    }

    public View getEstimation() {
        return estimation;
    }

    public View getRiderOptions() {
        return riderOptions;
    }

    public ImageButton getButtonsMainMenu() {
        return mainMenu;
    }

    public View getDriverProfile() {
        return driverProfile;
    }

    public void putValues(final ETripStatus value) {
        status.setText(value.getTitleResId());
        //statusIcon.setImageResource(value.getIconResId());
    }
}
