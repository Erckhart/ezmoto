package com.ezmoto.controller.net.conn;

import com.ezmoto.controller.net.BaseConn;
import com.ezmoto.controller.net.exception.NetworkStatusException;
import com.ezmoto.model.bo.DriverBO;
import com.ezmoto.model.enums.ECommand;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.net.MalformedURLException;

public class ConnCallDriver extends BaseConn {

    private LatLng mPosition;

    public ConnCallDriver(final LatLng position) {
        mPosition = position;
    }

    @Override
    public ECommand getCommand() throws MalformedURLException {
        return ECommand.CALL_DRIVER;
    }

    @Override
    public void execute() throws NetworkStatusException {
        connect(METHOD_POST, MEDIUM_TIMEOUT, mountData());
    }

    @Override
    public DriverBO getResult() throws NetworkStatusException {
        final String data = getData();
        return new Gson().fromJson(data, DriverBO.class);
    }

    private String mountData() {
        final StringBuilder builder = new StringBuilder();
        builder.append("{");
        builder.append("\"latitude\":");
        builder.append(mPosition.latitude);
        builder.append(",");
        builder.append("\"longitude\":");
        builder.append(mPosition.longitude);
        builder.append("}");
        return builder.toString();
    }

}
