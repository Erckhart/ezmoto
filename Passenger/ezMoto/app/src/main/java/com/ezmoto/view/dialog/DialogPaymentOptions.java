package com.ezmoto.view.dialog;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;

import com.ezmoto.R;
import com.ezmoto.model.enums.EPaymentOptions;
import com.ezmoto.view.adapter.AdpPayments;
import com.ezmoto.view.holder.HolderDialogPaymentOptions;

public class DialogPaymentOptions extends BaseDialog implements AdapterView.OnItemClickListener {

    private final OnPaymentChooseListener mOnPaymentChooseListener;

    private HolderDialogPaymentOptions mHolder;
    private AdpPayments mAdpPayments;

    public DialogPaymentOptions(final Context context, final OnPaymentChooseListener onPaymentChooseListener) {
        super(context, R.layout.dialog_payment_options);
        mOnPaymentChooseListener = onPaymentChooseListener;
        initialize();
        initializeActions();
    }

    private void initializeActions() {
        mHolder.getPaymentOptions().setOnItemClickListener(this);
    }

    @Override
    protected void initialize() {
        mHolder = new HolderDialogPaymentOptions(mView);
        mAdpPayments = new AdpPayments(getContext());
        mHolder.getPaymentOptions().setAdapter(mAdpPayments);
    }

    @Override
    public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
        if (mOnPaymentChooseListener != null) {
            final EPaymentOptions option = mAdpPayments.getItem(position);
            if (option != null) {
                mOnPaymentChooseListener.onPaymentChoose(option);
            }
        }
        dismiss();
    }

    public interface OnPaymentChooseListener {
        void onPaymentChoose(final EPaymentOptions option);
    }

}
