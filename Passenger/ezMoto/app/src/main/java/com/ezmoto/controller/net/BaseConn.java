package com.ezmoto.controller.net;

import com.ezmoto.controller.net.exception.NetworkStatusException;
import com.ezmoto.model.enums.ECommand;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public abstract class BaseConn {

    private HttpURLConnection con;
    private static final String LINE = "\r\n";
    protected static final String METHOD_GET = "GET";
    protected static final String METHOD_POST = "POST";
    protected static final int MEDIUM_TIMEOUT = 5000;
    protected static final int MAX_TIMEOUT = 10000;
    public static final String UTF_8 = "UTF-8";

    /**
     * connection on server by URL
     *
     * @param method  post or get
     * @param timeout for connection
     * @param data    the json post
     * @throws NetworkStatusException when any exception was raised
     */
    protected void connect(final String method, final int timeout,
                           final String data) throws NetworkStatusException {
        try {
            final URL url = new URL(getCommand().getUrl());
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod(method);
            con.setRequestProperty("content-type", "application/json;  charset=utf-8");
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setConnectTimeout(timeout);
            if (method.equals(METHOD_POST))
                this.setData(data);
            con.connect();
        } catch (IOException e) {
            throw new NetworkStatusException();
        }
    }

    private void setData(final String rawData) throws IOException {
        if (con != null) {
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.write(rawData.getBytes(UTF_8));
            wr.flush();
            wr.close();
        }
    }

    /**
     * Return data of server InputStream
     *
     * @return {@link String}
     * @throws IOException
     */
    protected String getData() throws NetworkStatusException {
        String result = "";
        final StringBuffer buffer = new StringBuffer();
        try {
            InputStream mIs = con.getInputStream();
            BufferedReader mBr = new BufferedReader(new InputStreamReader(mIs,
                    UTF_8));
            String line = null;
            while ((line = mBr.readLine()) != null) {
                buffer.append(line + LINE);
            }
            mIs.close();
            con.disconnect();
            result = buffer.toString();

        } catch (IOException e) {
            throw new NetworkStatusException();
        }
        return result;
    }

    /**
     * abstract method for create URL for connect to server
     *
     * @throws MalformedURLException
     */
    public abstract ECommand getCommand() throws MalformedURLException;

    public abstract void execute() throws NetworkStatusException;

    public abstract Object getResult() throws NetworkStatusException;

}
