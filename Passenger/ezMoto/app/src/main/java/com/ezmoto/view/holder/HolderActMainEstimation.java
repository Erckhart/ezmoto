package com.ezmoto.view.holder;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.ezmoto.R;
import com.ezmoto.model.bo.EstimationBO;
import com.ezmoto.view.AbsHolderItem;
import com.ezmoto.view.widget.SlidingLayout;
import com.lsjwzh.widget.materialloadingprogressbar.CircleProgressBar;

public class HolderActMainEstimation extends AbsHolderItem<EstimationBO> {

    private TextView balance;
    private TextView duration;
    private Button callDriver;
    private Button cancelDriver;
    private Button startTrip;
    private CircleProgressBar progressBar;

    public HolderActMainEstimation(final View view) {
        super(view);
    }

    @Override
    public void putValues(final EstimationBO value) {
        final String balanceValue = String.format("%.2f", value.getBalance());
        balance.setText(balanceValue);
        duration.setText(getContext().getString(R.string.formatter_duration_estimation, value.getDuration()));
    }

    @Override
    protected void initializeFields() {
        balance = (TextView) getField(R.id.act_main_estimation_value);
        duration = (TextView) getField(R.id.act_main_estimation_duration);
        callDriver = (Button) getField(R.id.act_main_estimation_call_driver);
        progressBar = (CircleProgressBar) getField(R.id.act_main_estimation_progress);
        cancelDriver = (Button) getField(R.id.act_main_estimation_cancel_driver);
        startTrip = (Button) getField(R.id.act_main_estimation_start_trip);
    }

    public Button getCancelDriver() {
        return cancelDriver;
    }

    public Button getCallDriver() {
        return callDriver;
    }

    public Button getStartTrip() {
        return startTrip;
    }

    public CircleProgressBar getProgressBar() {
        return progressBar;
    }
}
