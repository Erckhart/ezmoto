package com.ezmoto.controller.util;

import java.util.HashMap;

public class SettingsUtil {

    public static final int TAXIMETER = 0;
    public static final int TAXIMETER_SIT = 1;

    private HashMap<Integer,String> settings;

    public static SettingsUtil _instance;

    public static SettingsUtil getInstance() {
        if(_instance == null) {
            _instance = new SettingsUtil();
        }
        return _instance;
    }

    private SettingsUtil() {
        settings = new HashMap<Integer,String>();
        loadSettings();
    }

    private void loadSettings() {
        settings.put(TAXIMETER, "1.4");
        settings.put(TAXIMETER_SIT, "4");
    }

    public void setValue(final int type, final String setting) {
        settings.put(type,setting);
    }

    public String getValue(final int type) {
        return settings.get(type);
    }

}
