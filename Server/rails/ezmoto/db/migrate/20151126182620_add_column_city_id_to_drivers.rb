class AddColumnCityIdToDrivers < ActiveRecord::Migration
  def change
    add_column :drivers, :city_id, :integer
  end
end
