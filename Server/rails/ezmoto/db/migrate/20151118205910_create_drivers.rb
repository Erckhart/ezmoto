class CreateDrivers < ActiveRecord::Migration
  def change
    create_table :drivers do |t|
      t.integer :userId
      t.string :carPlate
      t.datetime :birthday
      t.boolean :licensed
      t.string :licensedNumber
      t.string :avatar
      t.float :rate
      t.string :phoneNumber
      t.integer :cityId

      t.timestamps null: false
    end
  end
end
