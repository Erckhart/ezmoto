class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email, limit: 100
      t.string :password
      t.string :type, limit: 2
      t.string :status, limit: 2
      t.string :name, limte: 100

      t.timestamps null: false
    end
  end
end
