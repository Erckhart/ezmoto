class CreateCustumers < ActiveRecord::Migration
  def change
    create_table :custumers do |t|
      t.integer :userId
      t.datetime :birthday
      t.string :avatar
      t.float :rate
      t.string :phoneNumber

      t.timestamps null: false
    end
  end
end
