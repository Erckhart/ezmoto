class AddColumnCountryIdToProvinces < ActiveRecord::Migration
  def change
    add_column :provinces, :country_id, :integer
  end
end
