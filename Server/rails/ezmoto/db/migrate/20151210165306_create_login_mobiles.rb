class CreateLoginMobiles < ActiveRecord::Migration
  def change
    create_table :login_mobiles do |t|
      t.integer :user_id
      t.string :token_login
      t.timestamps null: false
    end
  end
end
