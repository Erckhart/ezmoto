class AddColumnLastNameToUser < ActiveRecord::Migration
  def change
    add_column :users, :last_name, :string, limit: 100
  end
end
