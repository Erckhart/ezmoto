class CreateProvinces < ActiveRecord::Migration
  def change
    create_table :provinces do |t|
      t.string :name, limit: 50
      t.string :initials, limit: 3

      t.timestamps null: false
    end
  end
end
