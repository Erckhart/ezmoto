# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151210165306) do

  create_table "cities", force: :cascade do |t|
    t.string   "name",        limit: 50
    t.string   "initials",    limit: 3
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "province_id", limit: 4
  end

  create_table "countries", force: :cascade do |t|
    t.string   "name",       limit: 50
    t.string   "initials",   limit: 3
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "custumers", force: :cascade do |t|
    t.integer  "userId",      limit: 4
    t.datetime "birthday"
    t.string   "avatar",      limit: 255
    t.float    "rate",        limit: 24
    t.string   "phoneNumber", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "drivers", force: :cascade do |t|
    t.integer  "userId",         limit: 4
    t.string   "carPlate",       limit: 255
    t.datetime "birthday"
    t.boolean  "licensed"
    t.string   "licensedNumber", limit: 255
    t.string   "avatar",         limit: 255
    t.float    "rate",           limit: 24
    t.string   "phoneNumber",    limit: 255
    t.integer  "cityId",         limit: 4
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.integer  "city_id",        limit: 4
  end

  create_table "login_mobiles", force: :cascade do |t|
    t.integer  "user_id",     limit: 4
    t.string   "token_login", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "provinces", force: :cascade do |t|
    t.string   "name",       limit: 50
    t.string   "initials",   limit: 3
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.integer  "country_id", limit: 4
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",      limit: 255
    t.string   "password",   limit: 255
    t.string   "name",       limit: 255
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "last_name",  limit: 100
    t.integer  "user_type",  limit: 4
    t.integer  "status",     limit: 4,   default: 1
  end

end
