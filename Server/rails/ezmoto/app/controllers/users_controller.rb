class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all.order('name')
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /mobile/users.json
  def create_mobile
    get_token
    @user = User.new(user_params)
    @error = ""
    
    respond_to do |format|
      if verify_token_to_create_user
        @user.status = 1
        if @user.save
          format_json_to_create_user(format)
        else
          @error = 'E00'
          if (@user.email.blank? or @user.password.blank? or @user.name.blank?)
            @error = 'E04'
          else
            if (!@user.email.blank?)
              unless @user.email =~ /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
                @error = 'E02'
              end
              has_user = User.where('email = :email', {email: @user.email}).limit(1)
              unless has_user.nil?
                @error = 'E01'
              end
            end
          end
          format.json do 
            render json: {
              code: @error
            }.to_json,
            status: :error
          end
        end
      else
        format.json do
          render json: {
            code: @error
          }.to_json,
          status: :error
        end
      end
    end
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    @error = ""

    respond_to do |format|
      # Possibles status to user is: 
      # 1 - Pending; 
      # 2 - Active;
      # 3 - Blocked;
      # 4 - Removed;
      @user.status = 1
        
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_user
    @user = User.find(params[:id])
  end

  def get_token
    @token = params[:token]
    puts " ##### TOKEN: #{@token}"
  end

  def format_json_to_create_user(format)
    # TODO
    unless is_not_json_request
      prepare_to_generate_token_login
      login = LoginMobile.new
      login.token_login = @user.get_token_login
      login.user_id = @user.id
      login.save
      format.json do
        render json: {
          token: login.token_login,
          id: @user.id,
          status: @user.status
        }.to_json,
        status: :ok
      end

    end
  end

  def is_not_json_request
    puts " ##### is_not_json_request #{request.path_parameters} FORMAT: #{request.path_parameters[:format]} "
    return (request.path_parameters.nil? or request.path_parameters[:format] != "json")
  end

  # Verify if token is valid
  def verify_token_to_create_user
    puts " ##### INTO verify_token_to_create_user"
    # make token verification
    is_valid = false

    if (@token != nil and @user.email != nil and @user.name != nil)
      tk_email = @user.email
      tk_name = @user.name.tr('AEIOUaeiou', '')
      
      tk_to_verify = Digest::MD5.hexdigest(tk_email +"-"+ tk_name)

      puts " ##### is it equals? #{@token} and  #{tk_to_verify}"
      is_valid = (@token == tk_to_verify)
      unless is_valid
        @error = "E03"
      end
    end

    is_valid
  
 end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:email, :password, :user_type, :status, :name, :last_name)
  end
    
  def set_user_types_collection
    @user_types = Hash.new("user_types")
    @user_types["1"] = "Customer"
    @user_types["2"] = "Driver"
  end

  def prepare_to_generate_token_login
     if !@user.nil? and !@user.id.nil?
       LoginMobile.destroy_all(user_id: @user.id)
    end
  end

end

