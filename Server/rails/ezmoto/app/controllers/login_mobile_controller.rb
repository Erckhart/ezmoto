class LoginMobileController < ApplicationController

  def login
    get_login_params

    respond_to do |format|
      if !@user.nil?
        # comment the line below if is not necessary
        clear_login_to_user

        login = LoginMobile.new
        login.token_login = @user.get_token_login
        login.user_id = @user.id
        if login.save
          format.json do
            render json: {
              token: login.token_login,
              name: @user.name,
              status: @user.status
            }.to_json,
            status: :ok
          end
        end
      else
        format.json do
          render json: {
            code: "E05"
          }.to_json,
          status: :error
        end
      end
    end
  end

  def logout
    get_logout_params
    # TODO remove token from user
    respond_to do |format|
      if @user_id.nil?
        Login.destroy_all(user_id: @user_id)
      end
      format.json { head :no_content }
   end
  end

  private
  def get_login_params
    # TODO get params from json resquest and get user
    email = params[:email]
    password = params[:password]
    has_user = User.where('email = :email and password = :pass', {email: email, pass: password}).limit(1)
    @user = User.new
    if !has_user.nil?
      @user = has_user[0]
    end
    
  end

  def clear_login_to_user
    if !@user.nil? and !@user.id.nil?
      LoginMobile.destroy_all(user_id: @user.id)
    end
  end

  def get_logout_params
    @user_token = parms[:token]
    email = params[:email]
    has_user = User.where('email = :email', {email: email})
    if !has_user.nil?
      @user_id = has_user[0].id
    end
  end
end
