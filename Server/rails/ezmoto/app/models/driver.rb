class Driver < ActiveRecord::Base
  belongs_to :city

  validates_presence_of :city_id, message: "is required"
  validates_presence_of :carPlate, message: "is required"
  validates_presence_of :licensed, message: "is required"
  validates_presence_of :phoneNumber, message: "is required"

end
