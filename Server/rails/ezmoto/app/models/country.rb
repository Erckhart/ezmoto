class Country < ActiveRecord::Base
   has_many :provinces
   
   validates_presence_of :name, message: "is required"
   validates_presence_of :initials, message: "is required"

   validates_uniqueness_of :initials, message: "already registred"
   validates_uniqueness_of :name, message: "already registred"

end
