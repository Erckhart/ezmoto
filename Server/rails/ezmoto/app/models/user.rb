class User < ActiveRecord::Base
  validates_presence_of :name, message: "is required" 
  validates_presence_of :password, message: "is required"
  validates_presence_of :user_type, message: "is required"
  validates_presence_of :status, message: "is required"
  validates_presence_of :email, message: "is required"

  validates_uniqueness_of :email, message: "already registred"
  validate :validate_email
  validate :validate_user_type
  validate :validate_status

  has_one :login_mobile

  public
  def get_token_login
    time_login = Time.now.strftime("%Y%m%d_%H.%M.%S")
    return Digest::MD5.hexdigest("app login time to user: #{time_login}-#{email}" )
  end

  private
  def validate_email
    errors.add("email", "is invalid") unless email =~ /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/
  end

  def validate_user_type
    errors.add("user_type", "is invalid") unless (user_type >= 1 and user_type <= 2)
  end

  def validate_status
    errors.add("status", "is invalid") unless (status >= 1 and status <= 4)
  end
end
