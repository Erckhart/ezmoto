json.array!(@drivers) do |driver|
  json.extract! driver, :id, :userId, :carPlate, :birthday, :licensed, :licensedNumber, :avatar, :rate, :phoneNumber, :cityId
  json.url driver_url(driver, format: :json)
end
