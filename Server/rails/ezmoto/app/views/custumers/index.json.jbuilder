json.array!(@custumers) do |custumer|
  json.extract! custumer, :id, :userId, :birthday, :avatar, :rate, :phoneNumber
  json.url custumer_url(custumer, format: :json)
end
