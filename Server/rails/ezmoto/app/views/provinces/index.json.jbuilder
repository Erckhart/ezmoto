json.array!(@provinces) do |province|
  json.extract! province, :id, :name, :initials, :country_id
  province_url(province, format: :json)
end
