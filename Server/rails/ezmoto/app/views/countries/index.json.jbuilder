json.array!(@countries) do |country|
  json.extract! country, :id, :name, :initials
   country_url(country, format: :json)
end
