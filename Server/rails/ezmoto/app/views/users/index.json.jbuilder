json.array!(@users) do |user|
  json.extract! user, :id, :email, :password, :user_type, :status, :name
  user_url(user, format: :json)
end
